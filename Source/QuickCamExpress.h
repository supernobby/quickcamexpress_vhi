/*
** QuickCamExpress.h
*/


#ifndef QUICKCAMEXPRESS_H
#define QUICKCAMEXPRESS_H


#include "Library.h"
#include <exec/execbase.h>
#include <utility/hooks.h>
#include <vhi/vhi.h>


/*
** struct to store camera identification data
*/
struct AutoBindData
{
  UWORD abd_VendorID ;
  UWORD abd_ProductID ;
  UWORD abd_BridgeID ;
} ;


/*
** runtime data related to the camera
*/
struct CameraContext
{
  const struct AutoBindData *cc_KnownCamera ;  /* our identification data */
  ULONG cc_Online ;  /* indicator, if camera is still online */
  struct PrivateBase *cc_PrivateBase ;  /* pointer to our library structure */
  struct ExecBase *cc_SysBase ;  /* store SysBase */
  struct Library *cc_PsdBase ;  /* store PsdBase */
  struct Task *cc_Task ;  /* store our task */
  ULONG cc_SignalBit ;  /* signal to inform the task on action */
  struct PsdDevice *cc_Device ;  /* store poseidon usb device */
  struct PsdInterface *cc_Interface;   /* usb interface */
  struct Hook cc_ReleaseHook ;  /* hook for release function */
  struct MsgPort *cc_MsgPort ;  /* message port for usb communication */
  struct PsdPipe *cc_ControlPipe ;  /* control pipe to the camera */
  UBYTE cc_ControlBuffer[ 0x30 ] ;  /* buffer to e. g. constuct i2c commands */
  struct PsdRTIsoHandler *cc_IsoHandler ;  /* poseidon iso handler */
  ULONG cc_IsoMaxPacketSize ;  /* packet size of the used iso endpoint */
  UBYTE *cc_IsoBuffer ;  /* the allocated iso buffer */
  UBYTE cc_IsoBufferLock ;  /* to lock the iso buffer */
  struct Hook cc_IsoRequestHook ;  /* hook to provide buffer for isochronous packet */
  struct Hook cc_IsoDoneHook ;  /* hook for completed isochronous transfer */
  UBYTE *cc_RAWBuffer ;  /* the allocated raw image buffer */
  ULONG cc_RAWBufferSize ;  /* raw image buffer size */
  ULONG cc_RAWBufferWriteIndex ;  /* current write position in the raw image buffer */
  UBYTE cc_RAWBufferLock ;  /* to lock the raw buffer if it is currently processed */
  UBYTE *cc_RGBBuffer ;  /* the allocated rgb demosaicked buffer */
  ULONG cc_Exposure ;  /* current exposure time */
  struct vhi_dimensions cc_Dimensions ;  /* vhi trust me image size settings */
  UBYTE cc_TrustMe ;  /* vhi trust me setting */
  LONG ( *cc_StartSensor )( struct CameraContext * ) ;
  LONG ( *cc_StopSensor )( struct CameraContext * ) ;
  LONG ( *cc_SetExposure )( struct CameraContext *, ULONG ) ;
  UBYTE cc_SensorAddress ;
} ;


/*
** possible bridge chips
*/
#define BRIDGE_NONE ( 0x0000 )
#define BRIDGE_STV0600 ( 0x0001 )
#define BRIDGE_STV0602AA ( 0x0002 )


/*
** public functions
*/
struct Library *CustomLibOpen( struct PrivateBase *PrivateBase ) ;
void CustomLibClose( struct PrivateBase *PrivateBase ) ;
ULONG REGFUNC vhi_api( REG( d2, ULONG method ), REG( d3, ULONG submethod ), REG( d4, APTR attr ), REG( d5, ULONG *errcode ), REG( d6, APTR vhi_handle), REG( a6, struct PrivateBase *PrivateBase ) ) ;


#endif  /* !QUICKCAMEXPRESS_H */
