/*
** Library.h
*/


#ifndef _LIBRARY_H
#define _LIBRARY_H


#include "quickcamexpress_vhi.h"
#include "CompilerExtensions.h"


/*
** "startup" function
*/
int DoNotCrashStartup( void ) ;


/*
** mandatory OS library functions
*/
ULONG LibReserved( void ) ;
BPTR LibExpunge( REG( a6, struct PrivateBase *PrivateBase ) ) ;
BPTR LibClose( REG( a6, struct PrivateBase *PrivateBase ) ) ;
struct Library *LibOpen( REG( a6, struct PrivateBase *PrivateBase ) ) ;


#endif  /* _LIBRARY_H */
