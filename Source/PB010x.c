/*
 * PB010x.c
 */


#include "PB010x.h"
#include "STV0602AA.h"
#include "DebugOutput.h"


/*
 * stop sensor to capture images
 */
static LONG StopSensor( struct CameraContext *MyCamera )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;

  Error = WriteSensor( MyCamera, PB_R7_ControlMode, ( 0UL << 1 /* CE */ ), 2 ) ;  /* clear chip enable */
  if( 0 == Error )
  {  /* clearing sensor chip enable ok */
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "stop sensor ok" ) ) ;
  }
  else
  {  /* clearing sensor chip enable failed */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "stop sensor failed" ) ) ;
  }

  return( Error ) ;
}


/*
 * start the sensor
 */
static LONG StartSensor( struct CameraContext *MyCamera )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;

  Error = WriteSensor( MyCamera, PB_R7_ControlMode, ( 1UL << 1 /* CE */ ), 2 ) ;  /* set chip enable */
  if( 0 == Error )
  {  /* setting sensor chip enable ok */
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "start sensor ok" ) ) ;
  }
  else
  {  /* setting sensor chip enable failed */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "start sensor failed" ) ) ;
  }

  return( Error ) ;
}


/*
 * initialize PB010x
 */
LONG InitPB010x( struct CameraContext *MyCamera )
{
 struct PrivateBase *PrivateBase ;
  LONG Error ;
  USHORT RegVal ;

  PrivateBase = MyCamera->cc_PrivateBase ;

  /* set I2C registers to 16-bit */
  Error = WriteBridge( MyCamera, STV_0x1423, 1, 1 ) ;
  if( !( Error ) )
  {  /* 16-bit I2C registers could be enabled */
    MyCamera->cc_SensorAddress = PB_I2C_ADDRESS ;
    Error = ReadSensor( MyCamera, PB_R0_ChipVersion, &RegVal, 2 ) ;
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "PB_R0_ChipVersion: 0x%lX", ( ULONG )RegVal ) ) ;
    if( ( !( Error ) ) && ( 0x6403 == RegVal ) )
    {  /* sensor identification ok */
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "PB010x sensor detected" ) ) ;
      MyCamera->cc_StartSensor = StartSensor ;
      MyCamera->cc_StopSensor = StopSensor ;
      MyCamera->cc_SetExposure = NULL ;  /* auto exposure control */
      WriteSensor( MyCamera, PB_R13_Reset, ( 1UL << 0 /* RESET */ ), 2 ) ;  /* put sensor control logic in reset state */
      WriteSensor( MyCamera, PB_R13_Reset, ( 0UL << 0 /* RESET */ ), 2 ) ;  /* end sensor control logic reset state */
      StopSensor( MyCamera ) ;  /* enable power down mode */
    }
    else
    {  /* sensor identification not ok */
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "no PB010x sensor found" ) ) ;
      Error = -1 ;
    }
  }
  else
  {  /* 16-bit I2C registers could not be enabled */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "16-bit I2C registers could not be enabled" ) ) ;
  }

  return( Error ) ;
}
