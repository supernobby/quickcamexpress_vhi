/*
** Demosaicking.c
*/


#include "Demosaicking.h"


/*
** create rgb image from raw image
*/
ULONG BayerDemosaicking( UBYTE *RAWData, struct vhi_image *VHIImage  )
{
  unsigned long Width, Height ;  /* width and height */
  UBYTE *RGBData ;
  unsigned long x, y ;  /* x and y coordinates */
  unsigned char r, g, b ;  /* calculated r g b values */
  unsigned long Brightness ;  /* return a brighness value */
  
  Width = VHIImage->width ;
  Height = VHIImage->height ;
  RGBData = VHIImage->chunky ;

  Brightness = 0 ;
  for( y = ( 0 + 0 ) ; y < ( Height - 0 ) ; y++ )
  {
    for( x = ( 0 + 0 ) ; x < ( Width - 0 ) ; x++ )
    {
      if( VHI_RGB_24 != VHIImage->type ) RGBData += 1 ;  /* skip the alpha component */
   
      if( y == 0 )
      {  /* top border */
        if( x == 0 )
        {  /* top left corner */
          r = ( ( RAWData[ +1 ] ) / 1 ) ;
          g = ( ( RAWData[ 0 ] ) / 1 ) ;
          b = ( ( RAWData[ +Width ] ) / 1 ) ;
        }
        else if( x == ( Width - 1 ) )
        {  /* top right corner */
          r = ( ( RAWData[ 0 ] ) / 1 ) ; ;
          g = ( ( RAWData[ -1 ] + RAWData[ +Width ] ) / 2 ) ;
          b = ( ( RAWData[ +Width - 1 ] ) / 1 ) ;
        }
        else
        {  /* top center */
          if( !( x & 1 ) )
          {  /* green pixel */
            r = ( ( RAWData[ -1 ] + RAWData[ +1 ] ) / 2 ) ;
            g = ( ( RAWData[ 0 ] ) / 1 ) ;
            b = ( ( RAWData[ +Width ] ) / 1 ) ;
          }
          else
          {  /* red pixel */
            r = ( ( RAWData[ 0 ] ) / 1 ) ;
            g = ( ( RAWData[ -1 ] + RAWData[ +1 ] ) / 2 ) ;
            b = ( ( RAWData[ +Width - 1 ] + RAWData[ +Width + 1 ] ) / 2 ) ; ;
          }
        }
      }
      else if( y == ( Height - 1 ) )
      {  /* bottom border */
        if( x == 0 )
        {  /* bottom left corner */
          r = ( ( RAWData[ -Width + 1 ] ) / 1 ) ;
          g = ( ( RAWData[ -Width ] + RAWData[ + 1 ] ) / 2 ) ;
          b = ( ( RAWData[ 0 ] ) / 1 ) ;
        }
        else if( x == ( Width - 1 ) )
        {  /* bottom right corner */
          r = ( ( RAWData[ -Width ] ) / 1 ) ;
          g = ( ( RAWData[ 0 ] ) / 1 ) ;
          b = ( ( RAWData[ -1 ] ) / 1 ) ;
        }
        else
        {  /* bottom center */
          if( !( x & 1 ) )
          {  /* blue pixel */
            r = ( ( RAWData[ -Width - 1 ] + RAWData[ -Width + 1 ] ) / 2 ) ; ;
            g = ( ( RAWData[ -1 ] + RAWData[ +1 ] ) / 2 ) ;
            b = ( ( RAWData[ 0 ] ) / 1 ) ;
          }
          else
          {  /* green pixel */
            r = ( ( RAWData[ -Width ] ) / 1 ) ;
            g = ( ( RAWData[ 0 ] ) / 1 ) ;
            b = ( ( RAWData[ -1 ] + RAWData[ +1 ] ) / 2 ) ;
          }
        }
      }
      else
      {  /* vertical center */
        if( x == 0 )
        {  /* left border */
          if( !( y & 1 ) )
          {  /* green pixel */
            r = ( ( RAWData[ +1 ] ) / 1 ) ; ;
            g = ( ( RAWData[ 0 ] ) / 1 ) ;
            b = ( ( RAWData[ -Width ] + RAWData[ +Width ] ) / 2 ) ;
          }
          else
          {  /* blue pixel */
            r = ( ( RAWData[ -Width + 1 ] + RAWData[ +Width + 1 ] ) / 2 ) ;
            g = ( ( RAWData[ -Width ] + RAWData[ +Width ] ) / 2 ) ;
            b = ( ( RAWData[ 0 ] ) / 1 ) ;
          }
        }
        else if( x == ( Width - 1 ) )
        {  /* right border */
          if( !( y & 1 ) )
          {  /* red pixel */
            r = ( ( RAWData[ 0 ] ) / 1 ) ; ;
            g = ( ( RAWData[ -Width ] + RAWData[ +Width ] ) / 2 ) ;
            b = ( ( RAWData[ -Width - 1 ] + RAWData[ +Width - 1 ] ) / 2 ) ;
          }
          else
          {  /* green pixel */
            r = ( ( RAWData[ -Width ] + RAWData[ +Width ] ) / 2 ) ;
            g = ( ( RAWData[ 0 ] ) / 1 ) ;
            b = ( ( RAWData[ -1 ] ) / 1 ) ; ;
          }
        }
        else
        {  /* hozizontal center */
          if( !( y & 1 ) )
          {  /* even row */
            if( !( x & 1 ) )
            {  /* green pixel */
              r = ( ( RAWData[ -1 ] + RAWData[ +1 ] ) / 2 ) ;
              g = ( ( RAWData[ 0 ] ) / 1 ) ;
              b = ( ( RAWData[ -Width ] + RAWData[ +Width ] ) / 2 ) ;
            }
            else
            {  /* red pixel */
              r = ( ( RAWData[ 0 ] ) / 1 ) ;
              g = ( ( RAWData[ -Width ] + RAWData[ -1 ] + RAWData[ +1 ] + RAWData[ +Width ] ) / 4 ) ;
              b = ( ( RAWData[ -Width - 1 ] + RAWData[ -Width + 1  ] + RAWData[ +Width - 1 ] + RAWData[ +Width + 1 ] ) / 4 ) ;
            }
          }
          else
          {  /* odd  row */ 
            if( !( x & 1 ) )
            {  /* blue pixel */
              r = ( ( RAWData[ -Width - 1 ] + RAWData[ -Width + 1  ] + RAWData[ +Width - 1 ] + RAWData[ +Width + 1 ] ) / 4 ) ;
              g = ( ( RAWData[ -Width ] + RAWData[ -1 ] + RAWData[ +1 ] + RAWData[ +Width ] ) / 4 ) ;
              b = ( ( RAWData[ 0 ] ) / 1 ) ;
            }
            else
            {  /* green pixel */
              r = ( ( RAWData[ -Width ] + RAWData[ +Width ] ) / 2 ) ;
              g = ( ( RAWData[ 0 ] ) / 1 ) ;
              b = ( ( RAWData[ -1 ] + RAWData[ +1 ] ) / 2 ) ;
            }
          }
        }
      }

      RGBData[ 0 ] = r ;
      RGBData[ 1 ] = g ;
      RGBData[ 2 ] = b ;

      Brightness += ( r + r + b + g + g + g ) / 6 ;
      
      RAWData += 1 ;
      RGBData += 3 ;
    }
  }

  if( Brightness ) Brightness /= ( ( ( Height - 0 ) ) * ( ( Width - 0 ) ) ) ; 
  
  return( Brightness ) ;
}
