/*
** DebugOutput.c
*/


#include "DebugOutput.h"
#include <devices/serial.h>
#include <proto/exec.h>
#include <stdarg.h>


/*
** get resources belonging to serial output context and set bautrate
*/
static void ConfigSerial( struct ExecBase *SysBase, unsigned long BaudRate )
{
  struct MsgPort *MyMsgPort ;
  struct IOExtSer *MyIOExtSer ;
  //struct ExecBase *sdc_SysBase ;

  MyMsgPort = CreateMsgPort( ) ;
  if( NULL != MyMsgPort )
  {  /* message port ok */
    MyIOExtSer = ( struct IOExtSer * )CreateIORequest( MyMsgPort , sizeof ( struct IOExtSer ) ) ;
    if( NULL != MyIOExtSer )
    {  /* io request ok */
      OpenDevice( "serial.device", 0, ( struct IORequest * )MyIOExtSer, 0 ) ;
      if( 0 == MyIOExtSer->IOSer.io_Error )
      {  /* serial device ok */
        MyIOExtSer->IOSer.io_Command = SDCMD_SETPARAMS ;
        MyIOExtSer->io_Baud = BaudRate ;
        MyIOExtSer->io_SerFlags &= ~( SERF_PARTY_ON ) ;
        MyIOExtSer->io_SerFlags |= ( SERF_XDISABLED ) ;
        MyIOExtSer->io_StopBits = 1 ;
        DoIO( ( struct IORequest * )MyIOExtSer ) ;
        /* all done */
        CloseDevice( ( struct IORequest * )MyIOExtSer ) ;
      }
      else
      {  /* serial device not ok */
      }
      DeleteIORequest( ( struct IORequest * )MyIOExtSer ) ;
    }
    else
    {  /* io request not ok */
    }
    DeleteMsgPort( MyMsgPort ) ;
  }
  else
  {  /* message port not ok */
  }

  return ;
}


/*
** init the debug output
*/
void InitDebugOutput( struct ExecBase *SysBase )
{
  ConfigSerial( SysBase, 115200 ) ;
}


/*
** debug output function in printf style
*/
void DebugPrintf( const char *FormatString, ... )
{
  struct ExecBase *SysBase ;
  static const UWORD DebugPutChar[ 5 ] = { 0xCD4B, 0x4EAE, 0xFDFC, 0xCD4B, 0x4E75 } ;
  va_list Values ;

  SysBase = *( (struct ExecBase ** )( 4UL ) ) ;
  va_start( Values, FormatString ) ;

  RawDoFmt( ( STRPTR )FormatString, ( APTR )Values, ( void ( * )( void ) )DebugPutChar, ( APTR )SysBase ) ;
  RawDoFmt( ( STRPTR )"\n", ( APTR )NULL, ( void ( * )( void ) )DebugPutChar, ( APTR )SysBase ) ;

  va_end( Values ) ;
}
