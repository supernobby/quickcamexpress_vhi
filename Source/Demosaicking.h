/*
** Demosaicking.h
*/


#ifndef DEMOSAICKING_H
#define DEMOSAICKING_H


#include <exec/types.h>
#include <vhi/vhi.h>


ULONG BayerDemosaicking( UBYTE *RAWData, struct vhi_image *VHIImage ) ;


#endif  /* !DEMOSAICKING_H */
