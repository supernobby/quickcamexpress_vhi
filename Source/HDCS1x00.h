/*
** HDCS1x00.h
*/


#ifndef _HDCS1X00_H
#define _HDCS1X00_H


#include "QuickCamExpress.h"


/*
** the sensor i2c address
*/
#define HDCS1X00_I2C_ADDRESS ( 0x55UL << 1 )  /* 0xAA */


/*
** register addesses
*/
#define HDCS1X00_IDENT ( 0x00UL << 1 )  /* 0x00 */
#define HDCS1X00_STATUS ( 0x01UL << 1 )  /* 0x2 */
#define HDCS1X00_IMASK ( 0x02UL << 1 )  /* 0x4 */
#define HDCS1X00_PCTRL ( 0x03UL << 1 )  /* 0x6 */
#define HDCS1X00_PDRV ( 0x04UL << 1 )  /* 0x8 */
#define HDCS1X00_ICTRL ( 0x05UL << 1 )  /* 0xA */
#define HDCS1X00_ITMG ( 0x06UL << 1 )  /* 0xC */
#define HDCS1X00_BFRAC ( 0x07UL << 1 )  /* 0xE */
#define HDCS1X00_BRATE ( 0x08UL << 1 )  /* 0x10 */
#define HDCS1X00_ADCCTRL ( 0x09UL << 1 )  /* 0x12 */
#define HDCS1X00_FWROW ( 0x0AUL << 1 )  /* 0x14 */
#define HDCS1X00_FWCOL ( 0x0BUL << 1 )  /* 0x16 */
#define HDCS1X00_LWROW ( 0x0CUL << 1 )  /* 0x18 */
#define HDCS1X00_LWCOL ( 0x0DUL << 1 )  /* 0x1A */
#define HDCS1X00_TCTRL ( 0x0EUL << 1 )  /* 0x1C */
#define HDCS1X00_ERECPGA ( 0x0FUL << 1 )  /* 0x1E */
#define HDCS1X00_EROCPGA ( 0x10UL << 1 )  /* 0x20 */
#define HDCS1X00_ORECPGA ( 0x11UL << 1 )  /* 0x22 */
#define HDCS1X00_OROCPGA ( 0x12UL << 1 )  /* 0x24 */
#define HDCS1X00_ROWEXPL ( 0x13UL << 1 )  /* 0x26 */
#define HDCS1X00_ROWEXPH ( 0x14UL << 1 )  /* 0x28 */
#define HDCS1X00_SROWEXPL (0x15UL << 1 )  /* 0x2A */
#define HDCS1X00_SROWEXPH ( 0x16UL << 1 )  /* 0x2C */
#define HDCS1X00_CONFIG ( 0x17UL << 1 )  /* 0x2E */
#define HDCS1X00_CONTROL ( 0x18UL << 1 )  /* 0x30 */


/*
** Identifications Register IDENT 0x00
*/
#define HDCS1X00_IDENT_REV ( 0x07U<<0 )
#define HDCS1X00_IDENT_TYPE ( 0x1FU<<1 )


/*
** Status Register STATUS 0x01
*/
#define HDCS1X00_STATUS_RF ( 1U<<0 )
#define HDCS1X00_STATUS_RC ( 1U<<1 )
#define HDCS1X00_STATUS_FC ( 1U<<2 )
#define HDCS1X00_STATUS_CC ( 1U<<3 )
#define HDCS1X00_STATUS_EEF ( 1U<<4 )
#define HDCS1X00_STATUS_IEF ( 1U<<5 )
#define HDCS1X00_STATUS_SSF ( 1U<<6 )


/*
** Interrupt Mask Register IMASK 0x02
*/
#define HDCS1X00_IMASK_IEN ( 1U<<0 )
#define HDCS1X00_IMASK_IRC ( 1U<<1 )
#define HDCS1X00_IMASK_IFC ( 1U<<2 )
#define HDCS1X00_IMASK_ICC ( 1U<<3 )
#define HDCS1X00_IMASK_IEE ( 1U<<4 )
#define HDCS1X00_IMASK_IIE ( 1U<<5 )
#define HDCS1X00_IMASK_ISS ( 1U<<6 )


/*
** Pad Control Register PCTRL 0x03
*/
#define HDCS1X00_PCTRL_RCE ( 1U<<0 )
#define HDCS1X00_PCTRL_FSE ( 1U<<1 )
#define HDCS1X00_PCTRL_FSS ( 1U<<2 )
#define HDCS1X00_PCTRL_ICE ( 1U<<3 )
#define HDCS1X00_PCTRL_IPD ( 1U<<4 )
#define HDCS1X00_PCTRL_LVR ( 1U<<5 )
#define HDCS1X00_PCTRL_LVF ( 1U<<6 )
#define HDCS1X00_PCTRL_LVC ( 1U<<7 )


/*
** Pad Drive Control Register PDRV 0x04
*/
#define HDCS1X00_PDRV_DATDRV_0 ( 1U<<0 )
#define HDCS1X00_PDRV_DATDRV_1 ( 1U<<1 )
#define HDCS1X00_PDRV_RDYDRV_0 ( 1U<<2 )
#define HDCS1X00_PDRV_RDYDRV_1 ( 1U<<3 )
#define HDCS1X00_PDRV_STATDRV_0 ( 1U<<4 )
#define HDCS1X00_PDRV_STATDRV_1 ( 1U<<5 )
#define HDCS1X00_PDRV_TXDDRV_0 ( 1U<<6 )
#define HDCS1X00_PDRV_TXDDRV_1 ( 1U<<7 )


/*
** Interface Control Register ICTRL 0x05
*/
#define HDCS1X00_ICTRL_AAD ( 1U<<0 )
#define HDCS1X00_ICTRL_DAD ( 1U<<1 )
#define HDCS1X00_ICTRL_DOD_0 ( 1U<<2 )
#define HDCS1X00_ICTRL_DOD_1 ( 1U<<3 )
#define HDCS1X00_ICTRL_DDO ( 1U<<4 )
#define HDCS1X00_ICTRL_DSC_0 ( 1U<<5 )
#define HDCS1X00_ICTRL_DSC_1 ( 1U<<6 )
#define HDCS1X00_ICTRL_HAVG ( 1U<<7 )


/*
** Interface Timing Register ITMG 0x06
*/
#define HDCS1X00_ITMG_RPC_0 ( 1U<<0 )
#define HDCS1X00_ITMG_RPC_1 ( 1U<<1 )
#define HDCS1X00_ITMG_RPC_2 ( 1U<<2 )
#define HDCS1X00_ITMG_DHC_0 ( 1U<<3 )
#define HDCS1X00_ITMG_DHC_1 ( 1U<<4 )
#define HDCS1X00_ITMG_DPS ( 1U<<5 )


/*
** 
*/
#define HDCS1X00_0 ( 1U<<0 )
#define HDCS1X00_1 ( 1U<<1 )
#define HDCS1X00_2 ( 1U<<2 )
#define HDCS1X00_3 ( 1U<<3 )
#define HDCS1X00_4 ( 1U<<4 )
#define HDCS1X00_5 ( 1U<<5 )
#define HDCS1X00_6 ( 1U<<6 )
#define HDCS1X00_7 ( 1U<<7 )


/*
** Configuration Register CONFIG 0x17
*/
#define HDCS1X00_CONFIG_MODE_0 ( 1U<<0 )
#define HDCS1X00_CONFIG_MODE_1 ( 1U<<1 )
#define HDCS1X00_CONFIG_SFC ( 1U<<2 )
#define HDCS1X00_CONFIG_CFC ( 1U<<3 )
#define HDCS1X00_CONFIG_CSS ( 1U<<4 )
#define HDCS1X00_CONFIG_RSS ( 1U<<5 )


/*
** Control Register CONTROL 0x18
*/
#define HDCS1X00_CONTROL_RST ( 1U<<0 )
#define HDCS1X00_CONTROL_SLP ( 1U<<1 )
#define HDCS1X00_CONTROL_RUN ( 1U<<2 )


/*
** functions exported by the module
*/
LONG InitHDCS1x00( struct CameraContext *MyCamera ) ;


#endif  /* !_HDCS1X00_H */
