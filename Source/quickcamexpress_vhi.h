/*
** quickcamexpress_vhi.h
*/


#ifndef _QUICKCAMEXPRESS_VHI_H
#define _QUICKCAMEXPRESS_VHI_H


#include <dos/dos.h>
#include <exec/libraries.h>


/*
 * version definitions
 */
#define QUICKCAMEXPRESS_VHI_MAJOR 0
#define QUICKCAMEXPRESS_VHI_MINOR 4
#define QUICKCAMEXPRESS_VHI_REVISION 0


/*
 * string helper definitions
 */
#define QUOTE(_str) #_str
#define EXPAND_AND_QUOTE(_str) QUOTE(_str)


/*
** version and id definitions
*/
#define NAME_STRING "quickcamexpress.vhi"
#define MAJOR_STRING EXPAND_AND_QUOTE( QUICKCAMEXPRESS_VHI_MAJOR )
#define MINOR_STRING EXPAND_AND_QUOTE( QUICKCAMEXPRESS_VHI_MINOR )
#define REVISION_STRING EXPAND_AND_QUOTE( QUICKCAMEXPRESS_VHI_REVISION )
#define DATE_STRING __DATE__
#define COPYRIGHTPERIOD_STRING "Copyright 2012-2023"
#define AUTHOR_STRING "Andreas Barth"
#define MISC_STRING "VHI driver for Logitech Quickcam Express"


/*
** the private library base structure
*/
struct PrivateBase
{
  struct Library pb_Library ;  /* we are actually a library */
  /* private data */
  struct ExecBase *pb_SysBase ;  /* pointer to exec libaray */
  BPTR pb_SegList ;  /* pointer to our segment list */
  ULONG pb_DebugLevel ;  /* global debug level */
} ;


#endif  /* !_QUICKCAMEXPRESS_VHI_H */
