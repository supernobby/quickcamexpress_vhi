/*
** STV0602AA.c
*/


#include "STV0602AA.h"
#include "DebugOutput.h"
#include <proto/poseidon.h>
#include <stdio.h>


/*
** init STV0602-AA
*/
LONG InitSTV0602AA( struct CameraContext *MyCamera )
{
#if 0
  /*
  ** mysterious values based in Linux source and Beagle logs 
  */
  WriteBridge( MyCamera, STV_0x1446, 0x01 ) ;
  WriteBridge( MyCamera, STV_0x0423, 0x00 ) ;
  WriteBridge( MyCamera, STV_0x1500, 0x1D ) ;
  WriteBridge( MyCamera, STV_0x1501, 0xB5 ) ;
  WriteBridge( MyCamera, STV_0x1502, 0xa8 ) ;
  WriteBridge( MyCamera, STV_0x1503, 0x95 ) ;
  WriteBridge( MyCamera, STV_0x1504, 0x07 ) ;
  WriteBridge( MyCamera, STV_0x1443, 0x20 ) ;
  WriteBridge( MyCamera, STV_0x15C1, 847 ) ;
  WriteBridge( MyCamera, STV_0x15C3, 0x01 ) ;
  WriteBridge( MyCamera, STV_0x1680, 0x0a ) ;
#endif
#if 0
  WriteBridge( MyCamera, STV_0x1440, 0x00 ) ;
  WriteBridge( MyCamera, STV_0x0423, 0x04 ) ;
  WriteBridge( MyCamera, STV_0x1500, 0x1D ) ;
  WriteBridge( MyCamera, STV_0x1443, 0x01 ) ;
  WriteBridge( MyCamera, STV_0x1443, 0x00 ) ;
  WriteBridge( MyCamera, STV_0x1446, 0x00 ) ;
  WriteBridge( MyCamera, STV_0x1504, 0x07 ) ;
  WriteBridge( MyCamera, STV_0x1503, 0x45 ) ;
  WriteBridge( MyCamera, STV_0x1500, 0x1D ) ;
  WriteBridge( MyCamera, STV_0x0423, 0x04 ) ;
  WriteBridge( MyCamera, STV_0x1500, 0x1D ) ;
  WriteBridge( MyCamera, STV_0x15C3, 0x02 ) ;
  WriteBridge( MyCamera, STV_0x15C1, 0x27B ) ;
  WriteBridge( MyCamera, STV_0x1680, 0x00) ;
  WriteBridge( MyCamera, STV_0x1446, 0x00 ) ;
  WriteBridge( MyCamera, STV_0x1501, 0xC2 ) ;
  WriteBridge( MyCamera, STV_0x1502, 0xB0 ) ;
#endif
#if 1
  if( BRIDGE_STV0602AA == MyCamera->cc_KnownCamera->abd_BridgeID )
  {  /* use the STV0602AA in STV0600 emulation mode */
    WriteBridge( MyCamera, STV_0x1446 /* STV_STV0600_EMULATION */, 0x01, 1 ) ;
  }
  WriteBridge( MyCamera, STV_0x1423 /* STV_I2C_FLUSH */, 0x00, 1 ) ;
  WriteBridge( MyCamera, STV_0x1440 /* STV_ISO_ENABLE */, 0x00, 1 ) ;
  WriteBridge( MyCamera, STV_0x0423 /* ? */, 0x00, 1 ) ;
  WriteBridge( MyCamera, STV_0x1500 /* ? */, 0x1D, 1 ) ;  /* essential for 0x870 */
  WriteBridge( MyCamera, STV_0x1501 /* ? */, 0xB5, 1 ) ;
  WriteBridge( MyCamera, STV_0x1502 /* ? */, 0xA8, 1 ) ;
  WriteBridge( MyCamera, STV_0x1503 /* ? */, 0x95, 1 ) ;
  WriteBridge( MyCamera, STV_0x1504 /* ? */, 0x07, 1 ) ;
  WriteBridge( MyCamera, STV_0x1443 /* STV_SCAN_RATE */, 0x20, 1 ) ;
  WriteBridge( MyCamera, STV_0x15C3 /* STV_Y_CTRL */, 0x01, 1 ) ;  /* essential for 0x870 */
  WriteBridge( MyCamera, STV_0x1680 /* STV_X_CTRL */, 0x0A, 1 ) ;  /* essential for 0x870 */
#endif

  return( 0 ) ;
}


/*
** start bridge to generate iso traffic
*/
LONG StartBridge( struct CameraContext *MyCamera )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;

  Error = WriteBridge( MyCamera, STV_0x1440 /* STV_ISO_ENABLE */, 1, 1 ) ;
  if( 0 == Error )
  {  /* setting bridge to run mode ok */
    MyCamera->cc_IsoBufferLock = 0 ;
  }
  else
  {  /* setting bridge to run mode failed */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "setting bridge to run mode failed\n" ) ) ;
  }

  return( Error ) ;
}


/*
** stop bridge to generate iso traffic
*/
LONG StopBridge( struct CameraContext *MyCamera )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;

  Error = WriteBridge( MyCamera, STV_0x1440 /* STV_ISO_ENABLE */, 0, 1 ) ;
  if( 0 == Error )
  {  /* disabling bridge run mode ok */
    MyCamera->cc_IsoBufferLock = 1 ;
  }
  else
  {  /* disabling bridge run mode failed */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "disabling bridge run mode failed" ) ) ;
  }

  return( Error ) ;
}



/*
 * read STV0602-AA
 */
static LONG ReadBridgeControlBuffer( struct CameraContext *MyCamera, ULONG Address, ULONG Length )
{
  struct PrivateBase *PrivateBase ;
  struct Library *PsdBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;
  PsdBase = MyCamera->cc_PsdBase ;

  //DEBUGPRINTF( DEBUGLEVEL_INFO, ( "ReadBridgeControlBuffer, Address: 0x%lX, Length: %ld", Address, Length ) ) ;

  psdPipeSetup( MyCamera->cc_ControlPipe, URTF_IN | URTF_DEVICE | URTF_VENDOR, 0x04, Address, 0 ) ;
  Error = psdDoPipe( MyCamera->cc_ControlPipe, MyCamera->cc_ControlBuffer, Length ) ;

  if( 0 == Error )
  {  /* reading bridge ok */
  }
  else
  {  /* reading bridge failed */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "reading bridge control buffer failed" ) ) ;
  }

  return( Error ) ;
}


/*
 * write current control buffer
 */
static LONG WriteBridgeControlBuffer( struct CameraContext *MyCamera, ULONG Address, ULONG Length )
{
  struct PrivateBase *PrivateBase ;
  struct Library *PsdBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;
  PsdBase = MyCamera->cc_PsdBase ;

  //DEBUGPRINTF( DEBUGLEVEL_INFO, ( "WriteBridgeControlBuffer, Address: 0x%lX, Length: %ld", Address, Length ) ) ;

  psdPipeSetup( MyCamera->cc_ControlPipe, URTF_OUT | URTF_DEVICE | URTF_VENDOR, 0x04, Address, 0 ) ;
  Error = psdDoPipe( MyCamera->cc_ControlPipe, MyCamera->cc_ControlBuffer, Length ) ;

  if( 0 == Error )
  {  /* writing bridge ok */
  }
  else
  {  /* writing bridge failed */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "writing bridge control buffer failed" ) ) ;
  }

  return( Error ) ;
}



/*
 * read STV0602-AA
 */
LONG ReadBridge( struct CameraContext *MyCamera, ULONG Address, UBYTE *Buffer, ULONG Length )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;

  Error = ReadBridgeControlBuffer( MyCamera, Address, Length ) ;

  if( 0 == Error )
  {  /* reading bridge ok */
    switch( Length )
    {
      case 1:
        *( ( UBYTE * )Buffer ) = ( MyCamera->cc_ControlBuffer[ 0x00 ] ) ;
        break ;
      case 2:
        *( ( USHORT * )Buffer ) = ( MyCamera->cc_ControlBuffer[ 0x00 ] ) | ( MyCamera->cc_ControlBuffer[ 0x01 ] << 8 ) ;
        break ;
      default:
        DEBUGPRINTF( DEBUGLEVEL_WARNING, ( "reading sensor unsupported length" ) ) ;
        Error = 1 ;
        break ;
    }
  }
  else
  {  /* reading bridge failed */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "reading bridge failed" ) ) ;
  }

  return( Error ) ;
}


/*
 * write STV0602-AA
 */
LONG WriteBridge( struct CameraContext *MyCamera, ULONG Address, ULONG Value, ULONG Length )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;

  DEBUGPRINTF( DEBUGLEVEL_DEBUG, ( "WriteBridge, Address: 0x%lX, Value: 0x%lX, Length: %ld", Address, Value, Length ) ) ;
  
  MyCamera->cc_ControlBuffer[ 0x00 ] = ( ( Value >> 0 ) & 0xFF ) ;
  if( 2 >= Length )
  {  /* two byte register */
    MyCamera->cc_ControlBuffer[ 0x01 ] = ( ( Value >> 8 ) & 0xFF ) ;
  }

  Error = WriteBridgeControlBuffer( MyCamera, Address, Length ) ;

  if( 0 == Error )
  {  /* writing bridge ok */
  }
  else
  {  /* writing bridge failed */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "writing bridge failed" ) ) ;
  }

  return( Error ) ;
}



/*
 * read sensor via i2c
 */
LONG ReadSensor( struct CameraContext *MyCamera, ULONG Address, UBYTE *Buffer, ULONG Length )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;

  DEBUGPRINTF( DEBUGLEVEL_INFO, ( "ReadSensor, Address: 0x%lX, Length: %ld", Address, Length ) ) ;
  
  // memset( MyCamera->cc_ControlBuffer, 0, STV_I2C_BUFFER_LENGTH ) ;
  MyCamera->cc_ControlBuffer[ 0 ] = ( ( Address ) | 0x01 ) ;
  MyCamera->cc_ControlBuffer[ 0x20 ] = ( MyCamera->cc_SensorAddress ) ;
  MyCamera->cc_ControlBuffer[ 0x21 ] = ( 1 - 1 ) ;  /* number of commands to send - 1 */
  MyCamera->cc_ControlBuffer[ 0x22 ] = STV_I2C_COMMAND_READ ;
  
  Error = WriteBridgeControlBuffer( MyCamera, STV_0x1400, STV_I2C_BUFFER_LENGTH ) ;
  
  if( 0 == Error )
  {  /* no error writing i2c address */
    Error = ReadBridge( MyCamera, STV_0x1410, Buffer, Length ) ;
    if( 0 == Error )
    {  /* reading sensor ok */
    }
    else
    {  /* reading sensor failed */
      DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "reading sensor stage 2 failed" ) ) ;
    }
  }
  else
  {  /* some error writing i2c address */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "reading sensor stage 1 failed" ) ) ;
  }

  return( Error ) ;
}


/*
 * write sensor via i2c
 */
LONG WriteSensor( struct CameraContext *MyCamera, ULONG Address, ULONG Value, ULONG Length )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;
  
  PrivateBase = MyCamera->cc_PrivateBase ;

  DEBUGPRINTF( DEBUGLEVEL_DEBUG, ( "WriteSensor, Address: 0x%lX, Value: 0x%lX, Length: %ld", Address, Value, Length ) ) ;
  // memset( MyCamera->cc_ControlBuffer, 0, STV_I2C_BUFFER_LENGTH ) ;
  MyCamera->cc_ControlBuffer[ 0 ] = ( Address ) ;
  MyCamera->cc_ControlBuffer[ 0x10 ] = ( ( Value >> 0 ) & 0xFF ) ;
  if( 2 >= Length )
  {  /* two byte register */
    MyCamera->cc_ControlBuffer[ 0x11 ] = ( ( Value >> 8 ) & 0xFF ) ;
  }
  MyCamera->cc_ControlBuffer[ 0x20 ] = ( MyCamera->cc_SensorAddress ) ;
  MyCamera->cc_ControlBuffer[ 0x21 ] = ( 1 - 1 ) ;  /* number of commands to send - 1 */
  MyCamera->cc_ControlBuffer[ 0x22 ] = STV_I2C_COMMAND_WRITE ;
  
  Error = WriteBridgeControlBuffer( MyCamera, STV_0x0400, STV_I2C_BUFFER_LENGTH ) ;

  if( 0 == Error )
  {  /* no error writing i2c data */
  }
  else
  {  /* some error writing i2c data */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "writing sensor failed" ) ) ;
  }

  return( Error ) ;
}

