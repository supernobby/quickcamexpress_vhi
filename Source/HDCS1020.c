/*
** HDCS1020.h
*/


#include "HDCS1020.h"
#include "STV0602AA.h"
#include "DebugOutput.h"
#include <stdio.h>


/*
** start sensor to capture images
*/
static LONG StartSensor( struct CameraContext *MyCamera )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;

  Error = WriteSensor( MyCamera, HDCS_CONTROL, HDCS_CONTROL_RUN, 1 ) ;  /* clear power down and set run mode */
  if( 0 == Error )
  {  /* setting sensor to run mode ok */
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "start sensor ok" ) ) ;
  }
  else
  {  /* setting sensor to run mode failed */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "start sensor failed" ) ) ;
  }

  return( Error ) ;
}


/*
** stop sensor to capture images
*/
static LONG StopSensor( struct CameraContext *MyCamera )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;

  Error = WriteSensor( MyCamera, HDCS_CONTROL, 0, 1 ) ;  /* clear run mode */
  if( 0 == Error )
  {  /* disabling sensor run mode ok */
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "stop sensor ok" ) ) ;
  }
  else
  {  /* disabling sensor run mode failed */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "stop sensor failed" ) ) ;
  }

  return( Error ) ;
}



/*
 * some timing values
 */
#define F ( 25 )  /* system clock frequency in MHz */
#define HB ( 0 )  /* horizontal blanking period, HBLANK reg */
#define RS ( 155 )  /* row sample period constant */
#define CPO ( 3 )  /* column processing overhead */
#define NCP ( 352 )  /* number of columns processed */
#define CTO ( 3 )  /* column timing overhead */
#define PS ( 11 )  /* PGA sample signal duration, PSMP field of TCTRL reg */
#define AS ( 5 )  /* ADC start signal duration, ASTRT field of TCTRL reg */
#define SCSF ( 2 )  /* single channel scale factor */
#define CT ( CTO + PS + AS )  /* column timing period */
#define CP ( CPO + ( NCP * CT / SCSF ) )  /* column processing period */
#define RP ( HB + RS + CP )  /* row processing period */
#define ER ( 96 )  /* exposure reset duration */
#define MNCT ( ( ER + 12 + ( CT - 1 ) ) / CT )


/*
ROWEXP = {ROWEXPH, ROWEXPL} = quotient {TEXP / (T * RP)}
  T = system clock period
  RP = HB+RS + CP
    HB = 0 (default HBLANK value)
    RS = 155
    CP = CPO + (NCP * CT / SCSF)
      CPO = 3
      NCP = 352
      CT = CTO + PS + AS
        CTO = 3
        PS = defined by PSMP field of TCTRL reg
        AS = defined by the ASTRT field of TCTRL reg.
      SCSF = 1

SREXP = TEXP / T - (ROWEXP * RP)
SROWEXP = quotient((NCP*CT-SREXP-ER-13)/(4*CT)
*/
static LONG SetExposure( struct CameraContext *MyCamera, ULONG NanoSeconds )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;
  LONG ROWEXP ;
  LONG SROWEXP ;

  PrivateBase = MyCamera->cc_PrivateBase ;
  Error = 0 ;

  ROWEXP = NanoSeconds * F / RP ;
  if( 32767 < ROWEXP )
  {
    ROWEXP = 32767 ;
  }
  SROWEXP = ( NCP * CT - ( ( NanoSeconds * F ) - ( ROWEXP * RP ) ) - ER - 13 ) / ( 4 * CT ) ;
  if( 0 > SROWEXP )
  {
    SROWEXP = 0 ;
  }
  else if( ( ( NCP - MNCT ) / 4 ) < SROWEXP )
  {
    SROWEXP = ( ( NCP - MNCT ) / 4 ) ;
  }

  DEBUGPRINTF( DEBUGLEVEL_DEBUG, ( "NanoSeconds: %ld, ROWEXP: %ld, SROWEXP: %ld", NanoSeconds, ROWEXP, SROWEXP ) ) ;
  WriteSensor( MyCamera, HDCS_ROWEXPL, ( ( ROWEXP >> 0 ) & 0xFF ), 1 ) ;
  WriteSensor( MyCamera, HDCS_ROWEXPH, ( ( ROWEXP >> 8 ) & 0xFF ), 1 ) ;
  WriteSensor( MyCamera, HDCS_SROWEXP, ( SROWEXP >> 2 ), 1 ) ;
  WriteSensor( MyCamera, HDCS_ERROR, HDCS_ERROR_EEF, 1 ) ;

  return( Error ) ;
}


/*
** set brightness
*/
#define BRIGHTNESS_IDEAL ( 128 )
static LONG AdjustExposure( struct CameraContext *MyCamera, ULONG Now )
{
  LONG Error = 0 ;
  ULONG NewExposure ;
  UBYTE Commands[ 12 ] ;
  UBYTE Last ;
  Last = Now ;
  
  NewExposure = MyCamera->cc_Exposure ;
  if( ( 100 - ( 40 / MyCamera->cc_Exposure ) ) > Last )
  {  /* too dark */
    NewExposure += MyCamera->cc_Exposure / ( ( Last - 0 ) / 4 + 1 ) + 1 ;
    if( 0x200 < NewExposure ) NewExposure = 0x200 ;
  }
  if( ( 120 + ( 40 / MyCamera->cc_Exposure ) ) < Last )
  {  /* too bright */
    NewExposure -= MyCamera->cc_Exposure / ( ( 255 - Last ) / 4 + 1 ) + 1 ;
    if( 0x0001 > NewExposure ) NewExposure = 0x0001 ;
  }

  if( NewExposure != MyCamera->cc_Exposure )
  {
    MyCamera->cc_Exposure = NewExposure ;

    //Commands[ 0 ] = HDCS_CONTROL ;
    //Commands[ 1 ] = 0 ;
    WriteSensor( MyCamera, HDCS_CONTROL, 0, 1 ) ;
    //Commands[ 2 ] = HDCS_ROWEXPL ;
    //Commands[ 3 ] = ( ( MyCamera->cc_Exposure >> 0 ) & 0xFF ) ;
    WriteSensor( MyCamera, HDCS_ROWEXPL, ( ( MyCamera->cc_Exposure >> 0 ) & 0xFF ), 1 ) ;
    //Commands[ 4 ] = HDCS_ROWEXPH ;
    //Commands[ 5 ] = ( ( MyCamera->cc_Exposure >> 8 ) & 0xFF ) ;
    WriteSensor( MyCamera, HDCS_ROWEXPH, ( ( MyCamera->cc_Exposure >> 8 ) & 0xFF ), 1 ) ;
    //Commands[ 6 ] = HDCS_SROWEXP ;
    //Commands[ 7 ] = 0 ;
    //WriteSensor( MyCamera, HDCS_SROWEXP, 0, 1 ) ;
    //Commands[ 8 ] = HDCS_ERROR ;
    //Commands[ 9 ] = HDCS_ERROR_EEF ;
    WriteSensor( MyCamera, HDCS_ERROR, HDCS_ERROR_EEF, 1 ) ;
    //Commands[ 10 ] = HDCS_CONTROL ;
    //Commands[ 11 ] = HDCS_CONTROL_RUN ;
    WriteSensor( MyCamera, HDCS_CONTROL, HDCS_CONTROL_RUN, 1 ) ;
    
    //Error = WriteMultiI2C( MyCamera, &Commands[ 0 ], 6 ) ;
  }

  return( Error ) ;
}



#if 0
/*
** print some sensor registers
*/
void DumpSensor( struct CameraContext *MyCamera )
{
  UBYTE Value ;

  //ReadSensor( MyCamera, HDCS_IDENT, &Value ) ;
  //printf( "HDCS_IDENT: 0x%02X\n", Value  ) ;
  //ReadSensor( MyCamera, HDCS_STATUS, &Value  ) ;
  //printf( "HDCS_STATUS: 0x%02X\n", Value  ) ;
  //ReadSensor( MyCamera, HDCS_PCTRL, &Value  ) ;
  //printf( "HDCS_PCTRL: 0x%02X\n", Value  ) ;
  //ReadSensor( MyCamera, HDCS_ITMG, &Value  ) ;
  //printf( "HDCS_ITMG: 0x%02X\n", Value  ) ;
  //ReadSensor( MyCamera, HDCS_CONFIG, &Value  ) ;
  //printf( "HDCS_CONFIG: 0x%02X\n", Value  ) ;
  //ReadSensor( MyCamera, HDCS_CONTROL, &Value  ) ;
  //printf( "HDCS_CONTROL: 0x%02X\n", Value  ) ;
  ReadI2C( MyCamera, HDCS_FWROW, &Value  ) ;
  printf( "HDCS_FWROW: 0x%02X\n", Value  ) ;
  ReadI2C( MyCamera, HDCS_FWCOL, &Value  ) ;
  printf( "HDCS_FWCOL: 0x%02X\n", Value  ) ;
  ReadI2C( MyCamera, HDCS_LWROW, &Value  ) ;
  printf( "HDCS_LWROW: 0x%02X\n", Value  ) ;
  ReadI2C( MyCamera, HDCS_LWCOL, &Value  ) ;
  printf( "HDCS_LWCOL: 0x%02X\n", Value  ) ;
  ReadI2C( MyCamera, HDCS_ROWEXPL, &Value  ) ;
  printf( "HDCS_ROWEXPL: 0x%02X\n", Value  ) ;
  ReadI2C( MyCamera, HDCS_ROWEXPH, &Value  ) ;
  printf( "HDCS_ROWEXPH: 0x%02X\n", Value  ) ;
}
#endif


/*
** initialize HDCS1020
*/
LONG InitHDCS1020( struct CameraContext *MyCamera )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;
  UBYTE SensorIdent ;
  
  PrivateBase = MyCamera->cc_PrivateBase ;

  /* set I2C registers to 8-bit */
  Error = WriteBridge( MyCamera, STV_0x1423, 0, 1 ) ;
  if( !( Error ) )
  {  /* 8-bit I2C registers could be enabled */
    MyCamera->cc_SensorAddress = HDCS_I2C_ADDRESS ;
    Error = ReadSensor( MyCamera, HDCS_IDENT, &SensorIdent, 1 ) ;
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "HDCS_IDENT: 0x%lX", ( ULONG )SensorIdent ) ) ;
    if( ( !( Error ) ) && ( 0x10 == SensorIdent ) )
    {  /* sensor identification ok */
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "HDCS1020 sensor detected" ) ) ;
      MyCamera->cc_StartSensor = StartSensor ;
      MyCamera->cc_StopSensor = StopSensor ;
      MyCamera->cc_SetExposure = SetExposure ;
      WriteSensor( MyCamera, HDCS_CONTROL, HDCS_CONTROL_RST, 1 ) ;  /* do sensor reset */
      WriteSensor( MyCamera, HDCS_CONTROL, 0x00, 1 ) ;  /* disable default power down mode */
      WriteSensor( MyCamera, HDCS_CONFIG, /**/HDCS_CONFIG_CFC | HDCS_CONFIG_SFC, 1 ) ;  /*  */
      WriteSensor( MyCamera, HDCS_PCTRL, 0x63, 1 ) ;  /* enable some pins */
      WriteSensor( MyCamera, HDCS_PDRV, 0x00, 1 ) ;  /* high drive (5 ns) for all pins */
      WriteSensor( MyCamera, HDCS_TCTRL, ( ( ( AS - 2 ) << 6 ) | ( PS ) ), 1 ) ;  /*  */
      WriteSensor( MyCamera, HDCS_ICTRL, 0x00, 1 ) ;  /*  */
      WriteSensor( MyCamera, HDCS_ITMG, 0x16, 1 ) ;  /*  */
      WriteSensor( MyCamera, HDCS_STATUS, 0x7F, 1 ) ;  /* clear status register */
#if 0      
      WriteSensor( MyCamera, HDCS_ERECPGA, 0x32, 1 ) ;
      WriteSensor( MyCamera, HDCS_EROCPGA, 0x32, 1 ) ;
      WriteSensor( MyCamera, HDCS_ORECPGA, 0x32, 1 ) ;
      WriteSensor( MyCamera, HDCS_OROCPGA, 0x32, 1 ) ;
#endif

      WriteSensor( MyCamera, HDCS_FWROW, ( 0x08 >> 2 ), 1 ) ;  /* 8 */
      WriteSensor( MyCamera, HDCS_FWCOL, ( 0x1C >> 2 ), 1 ) ;  /* 28 */
      WriteSensor( MyCamera, HDCS_LWROW, ( 0x127 >> 2 ), 1 ) ;  /* 295 */
      WriteSensor( MyCamera, HDCS_LWCOL, ( 0x17B >> 2 ), 1 ) ;  /* 379 */

      SetExposure( MyCamera, MyCamera->cc_Exposure ) ;
    }
    else
    {  /* sensor identification not ok */
       DEBUGPRINTF( DEBUGLEVEL_INFO, ( "no HDCS1020 sensor found" ) ) ;
       Error = -1 ;
    }
  }
  else
  {  /* 8-bit I2C registers could not be enabled */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "8-bit I2C registers could not be enabled" ) ) ;
  }
  
  return( Error ) ;
}

