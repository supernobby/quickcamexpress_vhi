/*
** ab_stdlib.c
*/


#include "ab_stdlib.h"


long ab_atol( char *Buffer )
{
  long Result ;
  unsigned long BufferIndex ;

  Result = 0 ;
  BufferIndex = 0 ;
  while( ( '0' <= Buffer[ BufferIndex ] ) && ( '9' >= Buffer[ BufferIndex ] ) )
  {
    Result *= 10 ;
    Result += Buffer[ BufferIndex ] - '0' ;
    BufferIndex++ ;
  }

  return( Result ) ;
}
