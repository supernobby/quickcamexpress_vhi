/*
** STV0602AA.h
*/


#ifndef _STV0602AA_H
#define _STV0602AA_H


#include "QuickCamExpress.h"


/*
** i2c related values
*/
#define STV_I2C_BUFFER_LENGTH ( 0x23 )
#define STV_I2C_COMMAND_READ ( 0x3 )
#define STV_I2C_COMMAND_WRITE ( 0x1 )


/*
** register addesses
*/
#define STV_0x0400 ( 0x0400 )

#define STV_0x0423 ( 0x0423 )
#define STV_0x0424 ( 0x0424 )

#define STV_0x1400 ( 0x1400 )

#define STV_0x1410 ( 0x1410 )

#define STV_0x1423 ( 0x1423 )

#define STV_0x1440 ( 0x1440 )
#define STV_0x1443 ( 0x1443 )
#define STV_0x1444 ( 0x1444 )
#define STV_0x1445 ( 0x1445 )
#define STV_0x1446 ( 0x1446 )


#define STV_0x1500 ( 0x1500 )
#define STV_0x1501 ( 0x1501 )
#define STV_0x1502 ( 0x1502 )
#define STV_0x1503 ( 0x1503 )
#define STV_0x1504 ( 0x1504 )

#define STV_0x15C1 ( 0x15C1 )

#define STV_0x15C3 ( 0x15C3 )

#define STV_0x1680 ( 0x1680 )



#define STV_REG23 ( 0x0423 )

/* Control registers of the STV0600 ASIC */
#define STV_I2C_PARTNER ( 0x1420 )
#define STV_I2C_VAL_REG_VAL_PAIRS_MIN1 ( 0x1421 )
#define STV_I2C_READ_WRITE_TOGGLE ( 0x1422 )
#define STV_I2C_FLUSH ( 0x1423 )
#define STV_I2C_SUCC_READ_REG_VALS ( 0x1424 )

#define STV_ISO_ENABLE ( 0x1440 )
#define STV_SCAN_RATE ( 0x1443 )
#define STV_LED_CTRL ( 0x1445 )
#define STV_STV0600_EMULATION ( 0x1446 )
#define STV_REG00 ( 0x1500 )
#define STV_REG01 ( 0x1501 )
#define STV_REG02 ( 0x1502 )
#define STV_REG03 ( 0x1503 )
#define STV_REG04 ( 0x1504 )

#define STV_ISO_SIZE_L ( 0x15c1 )
#define STV_ISO_SIZE_H ( 0x15c2 )

/* Refers to the CIF 352x288 and QCIF 176x144 */
/* 1: 288 lines, 2: 144 lines */
#define STV_Y_CTRL ( 0x15c3 )

/* 0xa: 352 columns, 0x6: 176 columns */
#define STV_X_CTRL ( 0x1680 )



/*
** functions exported by the module
*/
LONG InitSTV0602AA( struct CameraContext *MyCamera ) ;
LONG ReadBridge( struct CameraContext *MyCamera, ULONG Address, UBYTE *Buffer, ULONG Length ) ;
LONG WriteBridge( struct CameraContext *MyCamera, ULONG Address, ULONG Value, ULONG Length ) ;
LONG ReadSensor( struct CameraContext *MyCamera, ULONG Address, UBYTE *Buffer, ULONG Length ) ;
LONG WriteSensor( struct CameraContext *MyCamera, ULONG Address, ULONG Value, ULONG Length ) ;
LONG WriteSensorMulti( struct CameraContext *MyCamera, void *Buffer, ULONG Count, ULONG Length ) ;
LONG StartBridge( struct CameraContext *MyCamera ) ;
LONG StopBridge( struct CameraContext *MyCamera ) ;


#endif  /* _STV0602AA_H */
