/*
** QuickCamExpress.c
*/


#include "QuickCamExpress.h"
#include "Demosaicking.h"
#include "STV0602AA.h"
#include "HDCS1x00.h"
#include "HDCS1020.h"
#include "PB010x.h"
#include "Config.h"
#include "DebugOutput.h"
#include "CompilerExtensions.h"
#include <proto/exec.h>
#include <proto/poseidon.h>
#include <stdio.h>
#include <string.h>


/*
** local functions
*/
static void FreeCamera( struct CameraContext *OldCamera ) ;
static struct CameraContext *AllocCamera( struct PrivateBase *PrivateBase, ULONG *ErrorCode ) ;
static LONG PauseCamera( struct CameraContext *MyCamera ) ;
static LONG ResumeCamera( struct CameraContext *MyCamera ) ;
static void StopCamera( struct CameraContext *MyCamera ) ;
static LONG StartCamera( struct CameraContext *MyCamera, struct vhi_dimensions *Dimension ) ;
static STRPTR vhi_string( struct CameraContext *MyCamera, UBYTE *MyString ) ;
static ULONG vhi_method_get( struct CameraContext *MyCamera, ULONG Item, APTR Attr, ULONG *ErrorCode ) ;
static ULONG vhi_method_set( struct CameraContext *MyCamera, ULONG Item, APTR Attr, ULONG *ErrorCode ) ;
static struct vhi_image *vhi_grab( struct CameraContext *MyCamera, struct vhi_digitize *Digitize, ULONG *ErrorCode ) ;
static ULONG vhi_method_perform( struct CameraContext *MyCamera, ULONG Item, APTR Attr, ULONG *ErrorCode ) ;


/*
** custom part of LibOpen
*/
struct Library *CustomLibOpen( struct PrivateBase *PrivateBase )
{
  struct ExecBase *SysBase ;
  struct Library *Result ;

  SysBase = PrivateBase->pb_SysBase ;
  Result = NULL ;

  InitDebugOutput( SysBase ) ;
  if( 0 != ReadIntVariable( SysBase, "QUICKCAMEXPRESS/DEBUGLEVEL", &PrivateBase->pb_DebugLevel, sizeof( PrivateBase->pb_DebugLevel ) ) )
  {  /* some error reading the variable, use default */
    PrivateBase->pb_DebugLevel = DEBUGLEVEL_OFF ;
  }
  Result = &PrivateBase->pb_Library ;

  DEBUGPRINTF( DEBUGLEVEL_INFO, ( "CustomLibOpen()" ) ) ;

  return( Result ) ;  
}


/*
** custom part of LibClose
*/
void CustomLibClose( struct PrivateBase *PrivateBase )
{
  struct ExecBase *SysBase ;

  DEBUGPRINTF( DEBUGLEVEL_INFO, ( "CustomLibClose()" ) ) ;

  SysBase = PrivateBase->pb_SysBase ;

}


/*
** all known cameras
*/
static const struct AutoBindData KnownCameras[] =
{
  { 0x046d, 0x0840, BRIDGE_STV0600 },
  { 0x046d, 0x0870, BRIDGE_STV0602AA },
  { 0x0000, 0x0000, BRIDGE_NONE },
} ;


/*
** gets called when camera disappears
*/
static void REGFUNC ReleaseHook( REG( a0, struct Hook *hook ), \
                                 REG( a2, APTR pab ), \
                                 REG( a1, struct CameraContext *MyCamera ) )
{
  struct PrivateBase *PrivateBase ;
  struct ExecBase *SysBase ;

  PrivateBase = MyCamera->cc_PrivateBase ;
  SysBase = MyCamera->cc_SysBase ;

  MyCamera->cc_Online = FALSE ;  /* camera gone */
  Signal( MyCamera->cc_Task, ( 1UL << MyCamera->cc_SignalBit ) ) ;
}


/*
** release camera and all resources
*/
static void FreeCamera( struct CameraContext *OldCamera )
{
  struct PrivateBase *PrivateBase ;
  struct ExecBase *SysBase ;
  struct Library *PsdBase ;
  struct PsdAppBinding *AppBinding ;

  if( NULL != OldCamera )
  {  /* context memory needs to be released */
    PrivateBase = OldCamera->cc_PrivateBase ;
    SysBase = OldCamera->cc_SysBase ;
    if( NULL != OldCamera->cc_PsdBase )
    {  /* poseidon library needs to be closed */
      PsdBase = OldCamera->cc_PsdBase ;
      if( 1  /* device could already be gone if camera was unplugged */ )
      {  /* device is still there */
        if( 0 != OldCamera->cc_SignalBit )
        {  /* signal bit need to be released */
          if( 1  /* application binding could already be gone if camera was unplugged */ )
          {
            if( NULL != OldCamera->cc_MsgPort )
            {  /* message port needs to be deleted */
              if( NULL != OldCamera->cc_ControlPipe )
              {  /* control pipe to the device needs to be closed */
                psdFreePipe( OldCamera->cc_ControlPipe ) ;
              }
              DeleteMsgPort( OldCamera->cc_MsgPort ) ;
            }
            psdGetAttrs( PGA_DEVICE, OldCamera->cc_Device, \
                         DA_Binding, ( ULONG )&AppBinding, \
                         TAG_END ) ;
            if( NULL != AppBinding )
            {  /* camera binding sill active */
              psdReleaseAppBinding( AppBinding ) ;  /* will call release hook */
            }
          }
          FreeSignal( ( LONG )OldCamera->cc_SignalBit ) ;
          DEBUGPRINTF( DEBUGLEVEL_INFO, ( "signal bit released" ) ) ;
        }
        /* device will be freed by poseidon automatically */
        DEBUGPRINTF( DEBUGLEVEL_INFO, ( "device will be freed by poseidon automatically" ) ) ;
      }
      CloseLibrary( PsdBase ) ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "poseidon library closed" ) ) ;
    }
    FreeMem( OldCamera, sizeof( struct CameraContext ) ) ;
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "camera context freed" ) ) ;
  }
}


/*
** allocate camera resources
*/
static struct CameraContext *AllocCamera( struct PrivateBase *PrivateBase, ULONG *ErrorCode )
{
  struct ExecBase *SysBase ;
  struct CameraContext *NewCamera ;
  struct Library *PsdBase ;
  struct PsdAppBinding *AppBinding ;
  struct List *ConfigList ;
  struct List *InterfaceList ;
  LONG Error ;

  SysBase = PrivateBase->pb_SysBase ;
  
  *ErrorCode = VHI_ERR_INTERNAL_ERROR ;  /* if error, this is default error code */
  NewCamera = AllocMem( sizeof( struct CameraContext ), MEMF_PUBLIC | MEMF_CLEAR ) ;
  if( NULL != NewCamera )
  {  /* camera context ok */
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "camera context ok" ) ) ;
    NewCamera->cc_PrivateBase = PrivateBase ;
    NewCamera->cc_SysBase = SysBase ;  /* store SysBase */
    PsdBase = OpenLibrary( "poseidon.library", 4 ) ;
    if( NULL != PsdBase )
    {  /* poseidon library ok */
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "poseidon library ok" ) ) ;
      NewCamera->cc_PsdBase = PsdBase ;  /* store PsdBase */
      NewCamera->cc_KnownCamera = ( const struct AutoBindData * )KnownCameras ;
      NewCamera->cc_Device = NULL ;
      while( 0 != NewCamera->cc_KnownCamera->abd_VendorID )
      {
        NewCamera->cc_Device = psdFindDevice( NewCamera->cc_Device,
                                              DA_VendorID, NewCamera->cc_KnownCamera->abd_VendorID,
                                              DA_ProductID,NewCamera->cc_KnownCamera->abd_ProductID,
                                              TAG_END ) ;
        if( NULL != NewCamera->cc_Device )
        {  /* known camera found */
          break ;
        }
        else
        {  /* no known camera found */
          NewCamera->cc_KnownCamera++ ;  /* check again with next known camera */
        }
      }
      if( NULL != NewCamera->cc_Device )
      {  /* a known camera was found */
        DEBUGPRINTF( DEBUGLEVEL_INFO, ( "a known camera was found" ) ) ;
        NewCamera->cc_Task = FindTask( NULL ) ;  /* store our task id */
        NewCamera->cc_SignalBit = AllocSignal( -1 ) ;
        if( 0 != NewCamera->cc_SignalBit )
        {  /* signal bit ok */
          DEBUGPRINTF( DEBUGLEVEL_INFO, ( "signal bit ok: 0x%lX", ( 1UL << NewCamera->cc_SignalBit ) ) ) ;
          NewCamera->cc_ReleaseHook.h_Entry = ( HOOKFUNC )ReleaseHook ;
          AppBinding = psdClaimAppBinding( ABA_Device, ( ULONG )NewCamera->cc_Device,
                                           ABA_ReleaseHook, (ULONG )&NewCamera->cc_ReleaseHook,
                                           ABA_UserData, ( ULONG )NewCamera,
                                           TAG_END ) ;
          if( NULL != AppBinding )
          {  /* could bind to camera */
            DEBUGPRINTF( DEBUGLEVEL_INFO, ( "could bind to camera" ) ) ;
            NewCamera->cc_MsgPort = CreateMsgPort( ) ;
            if( NULL != NewCamera->cc_MsgPort )
            {  /* message port ok */
              DEBUGPRINTF( DEBUGLEVEL_INFO, ( "message port ok" ) ) ;
              NewCamera->cc_ControlPipe = psdAllocPipe( NewCamera->cc_Device, NewCamera->cc_MsgPort, NULL ) ;
              if( NULL != NewCamera->cc_ControlPipe )
              {  /* control pipe ok */
                DEBUGPRINTF( DEBUGLEVEL_INFO, ( "control pipe ok" ) ) ;
                psdGetAttrs( PGA_DEVICE, NewCamera->cc_Device,
                             DA_ConfigList, ( ULONG )&ConfigList,
                             TAG_END ) ;
                if( !( IsListEmpty( ConfigList ) ) )
                {  /* there should be one configuration */
                  DEBUGPRINTF( DEBUGLEVEL_INFO, ( "there should be one configuration" ) ) ;
                  psdGetAttrs( PGA_CONFIG, ConfigList->lh_Head,
                               CA_InterfaceList, ( ULONG )&InterfaceList,
                               TAG_END ) ;
                  if( !( IsListEmpty( InterfaceList ) ) )
                  {  /* there should be one interface for this configuration */
                    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "there should be one interface for this configuration" ) ) ;
                    NewCamera->cc_Interface = ( struct PsdInterface * )InterfaceList->lh_Head ;
                    Error = InitSTV0602AA( NewCamera ) ;
                    if( !( Error ) )
                    {  /* bridge chip ok */
                      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "bridge chip ok" ) ) ;
                      NewCamera->cc_Exposure = 5000 ;
                      Error = InitHDCS1x00( NewCamera ) ;
                      if( ( Error ) )
                      {  /* try other sensor */
                        Error = InitHDCS1020( NewCamera ) ;
                      }
                      if( ( Error ) )
                      {  /* try other sensor */
                        Error = InitPB010x( NewCamera ) ;
                      }
                      if( !( Error ) )
                      {  /* sensor chip ok */
                        DEBUGPRINTF( DEBUGLEVEL_INFO, ( "sensor chip ok" ) ) ;
                        NewCamera->cc_Online = TRUE ;  /* all done */
                        *ErrorCode = 0 ;
                      }
                      else
                      {  /* sensor chip not ok */
                        DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "failed to initialize sensor" ) ) ;
                        FreeCamera( NewCamera ) ;
                        NewCamera = NULL ;
                      }
                    }
                    else
                    {  /* sensor chip not ok */
                      DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "failed to initialize bridge" ) ) ;
                      FreeCamera( NewCamera ) ;
                      NewCamera = NULL ;
                    }
                  }
                  else
                  {  /* no interface in this configuration */
                    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "no interface" ) ) ;
                    FreeCamera( NewCamera ) ;
                    NewCamera = NULL ;
                  }
                }
                else
                {  /* no configuration */
                  DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "no configuration" ) ) ;
                  FreeCamera( NewCamera ) ;
                  NewCamera = NULL ;
                }
              }
              else
              {  /* control pipe not ok */
                DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "could not allocate control pipe" ) ) ;
                FreeCamera( NewCamera ) ;
                NewCamera = NULL ;
              }
            }
            else
            {  /* message port not ok */
              DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "could not create message port" ) ) ;
              FreeCamera( NewCamera ) ;
              NewCamera = NULL ;
            }
          }
          else
          {  /* could not bind to camera */
            DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "could not claim binding" ) ) ;
            FreeCamera( NewCamera ) ;
            NewCamera = NULL ;
          }
        }
        else
        {  /* signal bit not ok */
          DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "could not allocate a signal bit" ) ) ;
          FreeCamera( NewCamera ) ;
          NewCamera = NULL ;
        }
      }
      else
      {  /* no known camera found */
        DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "no known camera found" ) ) ;
        FreeCamera( NewCamera ) ;
        NewCamera = NULL ;
        *ErrorCode = VHI_ERR_NO_HARDWARE ;
      }
    }
    else
    {  /* poseidon library not ok */
      DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "can't open poseidon.library" ) ) ;
      FreeCamera( NewCamera ) ;
      NewCamera = NULL ;
    }
  }
  else
  {  /* camera context not ok */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "can't allocate camera context" ) ) ;
  }

  return( NewCamera ) ;
}


/*
** pause
*/
static LONG PauseCamera( struct CameraContext *MyCamera )
{
  struct PrivateBase *PrivateBase ;
  struct Library *PsdBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;
  PsdBase = MyCamera->cc_PsdBase ;

  /* stop iso handler */
  Error = psdStopRTIso( MyCamera->cc_IsoHandler ) ;
  if( !( Error ) )
  {
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "iso handler stopped" ) ) ;
    /* stop bridge */
    Error = StopBridge( MyCamera ) ;
    if( !( Error ) )
    {
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "bridge stopped" ) ) ;
      /* stop sensor */
      Error = MyCamera->cc_StopSensor( MyCamera ) ;
      if( !( Error ) )
      {
        DEBUGPRINTF( DEBUGLEVEL_INFO, ( "sensor stopped" ) ) ;
      }
      else
      {
        DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "sensor not stopped" ) ) ;
        Error = -1 ;
      }
    }
    else
    {
      DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "bridge not stopped" ) ) ;
      Error = -1 ;
    }
  }
  else
  {
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "iso handler not stopped" ) ) ;
    Error = -1 ;
  }

  return( Error ) ;
}


/*
** resume
*/
static LONG ResumeCamera( struct CameraContext *MyCamera )
{
  struct PrivateBase *PrivateBase ;
  struct Library *PsdBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;
  PsdBase = MyCamera->cc_PsdBase ;

  MyCamera->cc_RAWBufferLock = 1 ;
  MyCamera->cc_IsoBufferLock = 0 ;

  Error = MyCamera->cc_StartSensor( MyCamera ) ;
  if( ( !( Error ) ) )
  {  /* setting sensor to run mode ok */
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "sensor started" ) ) ;
    Error = StartBridge( MyCamera ) ;
    if( ( !( Error ) ) )
    {  /* enabling iso transfer ok */
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "bridge started" ) ) ;
      Error = psdStartRTIso( MyCamera->cc_IsoHandler ) ;
      if( ( !( Error ) ) )
      {  /* iso handler started */
        DEBUGPRINTF( DEBUGLEVEL_INFO, ( "iso handler started" ) ) ;
      }
      else
      {  /* iso handler not started */
        DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "iso handler not started" ) ) ;
        Error = -1 ;
      }
    }
    else
    {  /* enabling iso transfer failed */
      DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "enabling iso transfer failed" ) ) ;
      Error = -1 ;
    }
  }
  else
  {  /* setting sensor to run mode failed */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "setting sensor to run mode failed" ) ) ;
    Error = -1 ;
  }

  return( Error ) ;
}


/*
** gets called when isochronous transfer starts
*/
static void REGFUNC IsoRequestHook( REG( a0, struct Hook *hook ), \
                                    REG( a2, struct IOUsbHWRTIso *urti ), \
                                    REG( a1, struct IOUsbHWBufferReq *ubr ) )
{
  struct CameraContext *MyCamera ;
  struct PrivateBase *PrivateBase ;
  struct ExecBase *SysBase ;

  MyCamera = hook->h_Data ;
  PrivateBase = MyCamera->cc_PrivateBase ;
  SysBase = MyCamera->cc_SysBase ;

  if( MyCamera->cc_IsoBufferLock )
  {  /* currently no space to handle more data */
    ubr->ubr_Length = 0 ;
  }
  else
  {
    if( MyCamera->cc_IsoMaxPacketSize < ubr->ubr_Length )
    {  /* this would not fit in our iso buffer */
      ubr->ubr_Length = MyCamera->cc_IsoMaxPacketSize ;
    }
    ubr->ubr_Buffer = MyCamera->cc_IsoBuffer ;
  }
}


/*
** gets called when isochronous transfer completes
*/
static void REGFUNC IsoDoneHook( REG( a0, struct Hook *hook ), \
                                 REG( a2, struct IOUsbHWRTIso *urti ), \
                                 REG( a1, struct IOUsbHWBufferReq *ubr ) )
{
  struct PrivateBase *PrivateBase ;
  struct CameraContext *MyCamera ;
  struct ExecBase *SysBase ;
  ULONG IsoBufferIndex ;
  USHORT ChunkID, ChunkLength ;

  MyCamera = hook->h_Data ;
  PrivateBase = MyCamera->cc_PrivateBase ;
  SysBase = MyCamera->cc_SysBase ;
  IsoBufferIndex = 0 ;
  
  if( ubr->ubr_Length )
  {  /* some data was received */
    while( ubr->ubr_Length > IsoBufferIndex )
    {  /* chunk "header" is at least 4 bytes */
      ChunkID = ( MyCamera->cc_IsoBuffer[ IsoBufferIndex + 0 ] << 8 ) | ( MyCamera->cc_IsoBuffer[ IsoBufferIndex + 1 ] << 0 ) ;
      ChunkLength = ( MyCamera->cc_IsoBuffer[ IsoBufferIndex + 2 ] << 8 ) | ( MyCamera->cc_IsoBuffer[ IsoBufferIndex + 3 ] << 0 ) ;
      IsoBufferIndex += 4 ;
      switch( ChunkID )
      {
        case 0x8001:  /* image start */
        case 0xC001:  /* image start */
          if( !( MyCamera->cc_IsoBufferLock ) )
          {  /* start collecting image data chunks */
            MyCamera->cc_RAWBufferWriteIndex = 0 ;
            MyCamera->cc_RAWBufferLock = 0 ;
          }
          break ;
        case 0x0200:  /* image data */
        case 0x4200:  /* image data */
          if( !( MyCamera->cc_RAWBufferLock ) )
          {  /* currently raw buffer is not locked, so take this data */
            CopyMem( &MyCamera->cc_IsoBuffer[ IsoBufferIndex ], &MyCamera->cc_RAWBuffer[ MyCamera->cc_RAWBufferWriteIndex ], ChunkLength ) ;
            MyCamera->cc_RAWBufferWriteIndex += ChunkLength ;
          }
          break ;
        case 0x8002:  /* image complete */
        case 0xC002:  /* image complete */
          if( !( MyCamera->cc_RAWBufferLock ) )
          {  /* currently raw buffer is not locked, so take this data */
            MyCamera->cc_IsoBufferLock = MyCamera->cc_RAWBufferLock = 1 ;
          }
          break ;
        default:  /* unknown chunk */
          break ;
      }
      IsoBufferIndex += ChunkLength ;
    }
  }
  else
  {  /* no data was received */
    if( MyCamera->cc_IsoBufferLock )
    {  /* one frame is complete */
      Signal( MyCamera->cc_Task, ( 1UL << MyCamera->cc_SignalBit ) ) ;
    }
  }
}


/*
** make camera inactive
*/
static void StopCamera( struct CameraContext *MyCamera )
{
  struct PrivateBase *PrivateBase ;
  struct ExecBase *SysBase ;
  struct Library *PsdBase ;
  struct List *AlternateInterfaceList ;
  ULONG InterfaceNumber, AlternateInterfaceNumber ;
  LONG NumTags ;

  PrivateBase = MyCamera->cc_PrivateBase ;
  SysBase = MyCamera->cc_SysBase ;
  PsdBase = MyCamera->cc_PsdBase ;

  NumTags = psdGetAttrs( PGA_INTERFACE, MyCamera->cc_Interface, \
                         IFA_InterfaceNum, ( ULONG )&InterfaceNumber, \
                         IFA_AlternateNum, ( ULONG )&AlternateInterfaceNumber, \
                         IFA_AlternateIfList, ( ULONG )&AlternateInterfaceList, \
                         TAG_END ) ;
  if( 0 < NumTags )
  {  /* could get info about interfaces */
    if( !( ( InterfaceNumber == 0 ) && ( AlternateInterfaceNumber == 0 ) ) )
    {  /* switch to the only alterantive interface 0/0 */
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "switch to the interface 0/0" ) ) ;
      MyCamera->cc_Interface = ( struct PsdInterface * )AlternateInterfaceList->lh_Head ;
      psdSetAltInterface( MyCamera->cc_ControlPipe, MyCamera->cc_Interface ) ;
    }
  }
  else
  {  /* could get info about interfaces */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "could not get info about interfaces during stop camera (%ld)", NumTags ) ) ;    
  }
  if( NULL != MyCamera->cc_IsoHandler )
  {  /* iso handler needs to be released */
    if( NULL != MyCamera->cc_IsoBuffer )
    {  /* iso buffer ok */
      if( NULL != MyCamera->cc_RAWBuffer )
      {  /* raw buffer ok */
        FreeVec( MyCamera->cc_RAWBuffer ) ;
        MyCamera->cc_RAWBuffer = NULL ;
        DEBUGPRINTF( DEBUGLEVEL_INFO, ( "raw buffer freed" ) ) ;
      }
      FreeVec( MyCamera->cc_IsoBuffer ) ;
      MyCamera->cc_IsoBuffer = NULL ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "iso buffer freed" ) ) ;
    }
    psdFreeRTIsoHandler( MyCamera->cc_IsoHandler ) ;
    MyCamera->cc_IsoHandler = NULL ;
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "iso handler freed" ) ) ;
  }
}


/*
** make camera active
*/
static LONG StartCamera( struct CameraContext *MyCamera, struct vhi_dimensions *Dimension )
{
  struct PrivateBase *PrivateBase ;
  struct ExecBase *SysBase ;
  struct Library *PsdBase ;
  struct List *AlternateInterfaceList ;
  LONG NumTags ;
  ULONG InterfaceNumber, AlternateInterfaceNumber ;
  ULONG TransferType ;
  struct List *EndpointList ;
  struct Node *EndpointNode ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;
  SysBase = MyCamera->cc_SysBase ;
  PsdBase = MyCamera->cc_PsdBase ;

  NumTags = psdGetAttrs( PGA_INTERFACE, MyCamera->cc_Interface,
                         IFA_InterfaceNum, ( ULONG )&InterfaceNumber,
                         IFA_AlternateNum, ( ULONG )&AlternateInterfaceNumber,
                         IFA_AlternateIfList, ( ULONG )&AlternateInterfaceList,
                         TAG_END ) ;
  if( ( 3 <= NumTags ) && ( NULL != AlternateInterfaceList ) )
  {  /* could get info about interfaces */
    if( !( ( InterfaceNumber == 0 ) && ( AlternateInterfaceNumber == 1 ) ) )
    {  /* switch to the only alterantive interface 0/1 */
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "switch to the interface 0/1" ) ) ;
      MyCamera->cc_Interface = ( struct PsdInterface * )AlternateInterfaceList->lh_Head ;
      psdSetAltInterface( MyCamera->cc_ControlPipe, MyCamera->cc_Interface ) ;
    }
    psdGetAttrs( PGA_INTERFACE, MyCamera->cc_Interface,
                 IFA_EndpointList, ( ULONG )&EndpointList,
                 TAG_END ) ;
    EndpointNode = EndpointList->lh_Head ;
    while( NULL != ( EndpointNode->ln_Succ ) )
    {  /* loop through all the endpoints for this interface */
      psdGetAttrs( PGA_ENDPOINT, EndpointNode,
                   EA_TransferType, ( ULONG )&TransferType,
                   TAG_END ) ;
      switch( TransferType )
      {  /* what type of endpoint is it */
        case USEAF_ISOCHRONOUS:  /* the image data endpoint */
          psdGetAttrs( PGA_ENDPOINT, EndpointNode,
                       EA_MaxPktSize, ( ULONG )&MyCamera->cc_IsoMaxPacketSize,
                       TAG_END ) ;
          DEBUGPRINTF( DEBUGLEVEL_INFO, ( "isochronous packet size: %ld", MyCamera->cc_IsoMaxPacketSize ) ) ;
          MyCamera->cc_IsoRequestHook.h_Entry = ( HOOKFUNC )IsoRequestHook ;
          MyCamera->cc_IsoRequestHook.h_Data = MyCamera ;
          MyCamera->cc_IsoDoneHook.h_Entry = ( HOOKFUNC )IsoDoneHook ;
          MyCamera->cc_IsoDoneHook.h_Data = MyCamera ;
          MyCamera->cc_IsoHandler = psdAllocRTIsoHandler( EndpointNode,
                                                          RTA_InRequestHook, ( ULONG )&MyCamera->cc_IsoRequestHook,
                                                          RTA_InDoneHook, ( ULONG )&MyCamera->cc_IsoDoneHook,
                                                          TAG_END ) ;
          break ;
        default:  /* don't care about this one */
          break ;
      }
      EndpointNode = EndpointNode->ln_Succ ;
    }
  }
  else
  {  /* could not get info about interfaces */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "could not get info about interfaces during start camera (%ld)", NumTags ) ) ;    
  }
  if( NULL != MyCamera->cc_IsoHandler )
  {  /* isochronous handler ok */
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "iso handler allocated" ) ) ;
    MyCamera->cc_IsoBuffer = AllocVec( MyCamera->cc_IsoMaxPacketSize, MEMF_PUBLIC ) ;
    if( NULL != MyCamera->cc_IsoBuffer )
    {  /* iso buffer ok */
      MyCamera->cc_RAWBufferSize = 1UL * ( Dimension->x2 - Dimension->x1 + 1 ) * ( Dimension->y2 - Dimension->y1 + 1 ) ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "x1: %ld, y1: %ld, x2: %ld, y2: %ld", Dimension->x1, Dimension->y1, Dimension->x2, Dimension->y2 ) ) ; 
      MyCamera->cc_RAWBuffer = AllocVec( MyCamera->cc_RAWBufferSize, MEMF_PUBLIC ) ;
      if( NULL != MyCamera->cc_RAWBuffer )
      {  /* raw buffer ok */
        DEBUGPRINTF( DEBUGLEVEL_INFO, ( "raw buffer allocated, %ld bytes", MyCamera->cc_RAWBufferSize ) ) ;
        Error = 0 ;
      }
      else
      {  /* raw buffer not ok */
        DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "could not allocate raw buffer" ) ) ;
        StopCamera( MyCamera ) ;
        Error = -1 ;
      }
    }
    else
    {  /* iso buffer not ok */
      DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "could not allocate iso buffer" ) ) ;
      StopCamera( MyCamera ) ;
      Error = -1 ;
    }
  }
  else
  {  /* isochronous handler not ok */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "could not allocate iso handler" ) ) ;
    StopCamera( MyCamera ) ;
    Error = -1 ;
  }
 
  return( Error ) ;  
}


/*
** to return vhi compatible strings
*/
static STRPTR vhi_string( struct CameraContext *MyCamera, UBYTE *MyString )
{
  struct ExecBase *SysBase ;
  UBYTE *VHIString ;
  
  SysBase = MyCamera->cc_SysBase ;
  VHIString = AllocVec( ( ULONG )( strlen( MyString ) + 1 ), ( MEMF_ANY | MEMF_CLEAR ) ) ;
  if( NULL != VHIString )
  {
    strcpy( VHIString, MyString ) ;
  }
  
  return( VHIString ) ;
}


/*
** get information/data from the camera
*/
static ULONG vhi_method_get( struct CameraContext *MyCamera, ULONG Item, APTR Attr, ULONG *ErrorCode )
{
  struct PrivateBase *PrivateBase ;
  ULONG Result ;
  struct vhi_size *MaximumSize ;
  struct vhi_dimensions *TrustMeDimensions ;
  STRPTR StingPointer ;  /* to avoid warning "cast does not match function type" */

  PrivateBase = MyCamera->cc_PrivateBase ;
  Result = 0 ;  /* the default result */
  
  switch( Item )
  {  /* what data shall we provide? */
    case VHI_CARD_NAME:
      StingPointer = vhi_string( MyCamera, "Quickcam Express" ) ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_CARD_NAME: %s", StingPointer ) ) ;
      Result = ( ULONG )StingPointer ;
      break ;
    case VHI_CARD_MANUFACTURER:
      StingPointer = vhi_string( MyCamera, "Logitech" ) ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_CARD_MANUFACTURER: %s", StingPointer ) ) ;
      Result = ( ULONG )StingPointer ;
      break ;
    case VHI_CARD_REVISION:
      Result = 0 ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_CARD_REVISION: %ld", Result ) ) ;
      break ;
    case VHI_CARD_VERSION:
      Result = 0 ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_CARD_VERSION: %ld", Result ) ) ;
      break ;
    case VHI_CARD_DRIVERAUTHOR:
      StingPointer = vhi_string( MyCamera, "supernobby" ) ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_CARD_DRIVERAUTHOR: %s", StingPointer ) ) ;
      Result = ( ULONG )StingPointer ;
      break ;
    case VHI_SUPPORTED_MODES:
      Result = ( ULONG )( VHI_MODE_COLOR ) ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_SUPPORTED_MODES: %ld", Result ) ) ;
      break ;
    case VHI_NUMBER_OF_INPUTS:
      Result = 1 ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_NUMBER_OF_INPUTS: %ld", Result ) ) ;
      break ;
    case VHI_NAME_OF_INPUT:
      switch( ( ULONG )Attr )
      {
        case 0:
          StingPointer = vhi_string( MyCamera, "CMOS sensor" ) ;
          DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_NAME_OF_INPUT(0): %s", StingPointer ) ) ;
          Result = ( ULONG )StingPointer ;
          break ;
      }
      break ;
    case VHI_SUPPORTED_OPTIONS:
      Result = ( ULONG )0 ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_SUPPORTED_OPTIONS: %ld", Result ) ) ;
      break ;
    case VHI_SUPPORTED_VIDEOFORMATS:
      Result = ( ULONG )( VHI_FORMAT_PAL ) ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_SUPPORTED_VIDEOFORMATS: %ld", Result ) ) ;
      break ;
    case VHI_MAXIMUM_SIZE:
      MaximumSize = Attr ;
      if( NULL != MaximumSize )
      {  /* size struct to fill in ok */
        MaximumSize->max_width = 352 ;
        MaximumSize->max_height = 288 ;
        MaximumSize->fixed = TRUE ;
        MaximumSize->scalable = FALSE ;
        DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_MAXIMUM_SIZE: %ld", MaximumSize->max_width ) ) ;
      }
      else
      {  /* size struct to fill in not ok */
      }
      break ;
    case VHI_COLOR_MODE:
      Result = ( ULONG )1 ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_COLOR_MODE: %ld", Result ) ) ;
      break ;
    case VHI_TRUSTME_MODE:
      Result = ( ULONG )MyCamera->cc_TrustMe ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_TRUSTME_MODE: %ld", Result ) ) ;
      break ;
    case VHI_TRUSTME_SIZE:
      TrustMeDimensions = Attr ;
      if( NULL != TrustMeDimensions )
      {  /* dimensions struct to fill in ok */
        TrustMeDimensions->x1 = MyCamera->cc_Dimensions.x1 ;
        TrustMeDimensions->y1 = MyCamera->cc_Dimensions.y1 ;
        TrustMeDimensions->x2 = MyCamera->cc_Dimensions.x2 ;
        TrustMeDimensions->y2 = MyCamera->cc_Dimensions.y2 ;
        TrustMeDimensions->dst_width = MyCamera->cc_Dimensions.dst_width ;
        TrustMeDimensions->dst_height = MyCamera->cc_Dimensions.dst_height ;
        DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_TRUSTME__SIZE: x1: %ld, y1: %ld, x2: %ld, y2: %ld", MyCamera->cc_Dimensions.x1, MyCamera->cc_Dimensions.y1, MyCamera->cc_Dimensions.x2, MyCamera->cc_Dimensions.y2 ) ) ; 
      }
      break ;
    case VHI_CARD_DRIVERVERSION:
      Result = ( ULONG )QUICKCAMEXPRESS_VHI_MAJOR ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_CARD_DRIVERVERSION: %ld", Result ) ) ;
      break ;
    case VHI_CARD_DRIVERREVISION:
      Result = ( ULONG )QUICKCAMEXPRESS_VHI_MINOR ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_CARD_DRIVERREVISION: %ld", Result ) ) ;
      break ;
    case VHI_DIGITAL_CAMERA:
      Result = ( ULONG )( VHI_DC_TYPE_NOCAM ) ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "get VHI_DIGITAL_CAMERA: %ld", Result ) ) ;
      break ;
    default:
      DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "vhi_method_get() unknown item: %ld", Item ) ) ;
      *ErrorCode = VHI_ERR_UNKNOWN_OPTION ;
      break ;
  }
  
  return( Result ) ;
}


/*
** set information/data in the camera
*/
static ULONG vhi_method_set( struct CameraContext *MyCamera, ULONG Item, APTR Attr, ULONG *ErrorCode )
{
  struct PrivateBase *PrivateBase ;
  ULONG Result ;
  struct vhi_setoptions *SetOption ;
  struct vhi_dimensions *TrustMeDimensions ;
  
  PrivateBase = MyCamera->cc_PrivateBase ;
  Result = 0 ;

  switch( Item )
  {  /* what data shall we take over? */
    case VHI_OPTIONS:
      SetOption = Attr ;
      if( NULL != SetOption )
      {
        switch( SetOption->option )
        {
          default:
            *ErrorCode = VHI_ERR_UNKNOWN_OPTION ;
            break ;
        }
      }
      break ;
    case VHI_COLOR_MODE:
      /* nothing to do, always color mode */
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "set VHI_COLOR_MODE: %ld", ( ULONG )Attr ) ) ;
      break ;
    case VHI_VIDEOFORMAT:
      /* nothing to do, always color mode */
     DEBUGPRINTF( DEBUGLEVEL_INFO, ( "set VHI_VIDEOFORMAT: %ld", ( ULONG )Attr ) ) ;
     break ;
    case VHI_TRUSTME_MODE:
      MyCamera->cc_TrustMe =( BOOL )( ( ( ULONG )Attr ) & 0xFF ) ;
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "set VHI_TRUSTME_MODE: %d", MyCamera->cc_TrustMe ) ) ;
      break ;
    case VHI_TRUSTME_SIZE:
      TrustMeDimensions = Attr ;
      if( NULL != TrustMeDimensions )
      {  /* dimensions struct to read ok */
        MyCamera->cc_Dimensions.x1 = TrustMeDimensions->x1 ;
        MyCamera->cc_Dimensions.y1 = TrustMeDimensions->y1 ;
        MyCamera->cc_Dimensions.x2 = TrustMeDimensions->x2 ;
        MyCamera->cc_Dimensions.y2 = TrustMeDimensions->y2 ;
        MyCamera->cc_Dimensions.dst_width = TrustMeDimensions->dst_width ;
        MyCamera->cc_Dimensions.dst_height = TrustMeDimensions->dst_height ;
      }
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "set VHI_TRUSTME__SIZE: x1: %ld, y1: %ld, x2: %ld, y2: %ld", MyCamera->cc_Dimensions.x1, MyCamera->cc_Dimensions.y1, MyCamera->cc_Dimensions.x2, MyCamera->cc_Dimensions.y2 ) ) ; 
      break ;
    default:
      DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "vhi_method_set() unknown item: %ld", Item ) ) ;
      *ErrorCode = VHI_ERR_UNKNOWN_OPTION ;
      break ;
  }
  
  return( Result ) ;
}



/*
** adjust exposure time based on image brightness
*/
#define BRIGHTNESS_IDEAL ( 100 )
static LONG AdjustBrightness( struct CameraContext *MyCamera, ULONG Brighness )
{
  LONG Error = 0 ;
  ULONG NewExposure ;

  if( MyCamera->cc_SetExposure )
  {  /* camera uses manual exposure control */
    if( ( BRIGHTNESS_IDEAL ) > Brighness )
    {  /* too dark */
      NewExposure = MyCamera->cc_Exposure + ( ( ( BRIGHTNESS_IDEAL - Brighness ) * MyCamera->cc_Exposure / 200 ) ) ;
      if( 2000000 < NewExposure )
      {  /* limit to 2 seconds */
        NewExposure = 2000000 ;
      }
    }
    else if( ( BRIGHTNESS_IDEAL ) < Brighness )
    {  /* too bright */
      NewExposure = MyCamera->cc_Exposure - ( ( ( Brighness - BRIGHTNESS_IDEAL ) * MyCamera->cc_Exposure / 200 ) ) ;
      if( 1000 > NewExposure )
      {  /* limit to 1 milli second */
        NewExposure = 1000 ;
      }
    }
    else
    {  /* not too dark and not too bright */
      NewExposure = MyCamera->cc_Exposure ;
    }

    if( NewExposure != MyCamera->cc_Exposure )
    {
      Error = MyCamera->cc_SetExposure( MyCamera, NewExposure ) ;
      MyCamera->cc_Exposure = NewExposure ;
    }
  }
  else
  {  /* camera uses auto exposure control */
  }

  return( Error ) ;
}


/*
** digitize
*/
static struct vhi_image *vhi_grab( struct CameraContext *MyCamera, struct vhi_digitize *Digitize, ULONG *ErrorCode )
{
  struct PrivateBase *PrivateBase ;
  struct ExecBase *SysBase ;
  struct vhi_image *GrabedImage ;
  ULONG Brightness ;
  LONG Error ;
  
  PrivateBase = MyCamera->cc_PrivateBase ;
  SysBase = MyCamera->cc_SysBase ;
  GrabedImage = NULL ;
  
  if( MyCamera->cc_TrustMe )
  {  /* use trust me dimensions */
    Digitize->dim.x1 = MyCamera->cc_Dimensions.x1 ;
    Digitize->dim.y1 = MyCamera->cc_Dimensions.y1 ;
    Digitize->dim.x2 = MyCamera->cc_Dimensions.x2 ;
    Digitize->dim.y2 = MyCamera->cc_Dimensions.y2 ;
    Digitize->dim.dst_width = MyCamera->cc_Dimensions.dst_width ;
    Digitize->dim.dst_height = MyCamera->cc_Dimensions.dst_height ;
  }
  else
  {  /* check supplied dimensions */
    vhi_method_perform( MyCamera, VHI_CHECK_DIGITIZE_SIZE, &Digitize->dim, ErrorCode ) ;
  }
  
  if( 0 == StartCamera( MyCamera, &Digitize->dim ) )
  {  /* camera started grabbing */
    if( Digitize->custom_memhandling )
    {  /* use custom functions */
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "custom memhandling" ) ) ;
      GrabedImage = Digitize->CstAllocVec( sizeof( struct vhi_image ), ( MEMF_ANY | MEMF_CLEAR ) ) ;
      if( NULL != GrabedImage )
      {  /* custom allocated vhi image struct ok */
        GrabedImage->chunky = Digitize->CstAllocVec( 3UL * ( Digitize->dim.x2 - Digitize->dim.x1 + 1 ) * ( Digitize->dim.y2 - Digitize->dim.y1 + 1 ), MEMF_PUBLIC ) ;
        if( NULL != GrabedImage->chunky )
        {  /* custom allocated rgb image buffer ok */
        }
        else
        {  /* custom allocated rgb image buffer not ok */
          DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "custom allocated rgb image buffer not ok" ) ) ;
          Digitize->CstFreeVec( GrabedImage ) ;
          GrabedImage = NULL ;
        }
      }
      else
      { /* custom allocated vhi image struct not ok */
        DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "custom allocated vhi image struct not ok" ) ) ;
      }
    }
    else
    {  /* use exec functions */
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "exec memhandling" ) ) ;
      GrabedImage = AllocVec( sizeof( struct vhi_image ), ( MEMF_ANY | MEMF_CLEAR ) ) ;
      if( NULL != GrabedImage )
      {  /* exec allocated vhi image struct ok */
        GrabedImage->chunky = AllocVec( 3UL * ( Digitize->dim.x2 - Digitize->dim.x1 + 1 ) * ( Digitize->dim.y2 - Digitize->dim.y1 + 1 ), MEMF_PUBLIC ) ;
        if( NULL != GrabedImage->chunky )
        {  /* exec allocated rgb image buffer ok */
        }
        else
        {  /* exec allocated rgb image buffer ok */
          DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "exec allocated rgb image buffer not ok" ) ) ;
          FreeVec( GrabedImage ) ;
          GrabedImage = NULL ;
        }
      }
      else
      { /* exec allocated vhi image struct not ok */
        DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "exec allocated vhi image struct not ok" ) ) ;
      }
    }
    if( ( NULL != GrabedImage ) && ( NULL != GrabedImage->chunky ) )
    {  /* vhi image ok */
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "vhi image allocated" ) ) ;
      GrabedImage->width = Digitize->dim.x2 - Digitize->dim.x1 + 1 ;
      GrabedImage->height = Digitize->dim.y2 - Digitize->dim.y1 + 1 ;
      GrabedImage->scaled = 0 ;
      GrabedImage->type = VHI_RGB_24 ;
      GrabedImage->v = GrabedImage->u = GrabedImage->y = NULL ;
      Error = 0 ;
      Error = ResumeCamera( MyCamera ) ;
      if( !( Error ) )
      {  /* wait for end of grabbing */
        Wait( ( 1UL << MyCamera->cc_SignalBit ) ) ;
        if( TRUE == MyCamera->cc_Online )
        {  /* camera still there */
          Error = PauseCamera( MyCamera ) ;
          if( !( Error ) )
          {  /* camera could be paused */
            DEBUGPRINTF( DEBUGLEVEL_DEBUG, ( "raw buffer write index: %ld", MyCamera->cc_RAWBufferWriteIndex ) ) ;
            Brightness = BayerDemosaicking( MyCamera->cc_RAWBuffer, GrabedImage ) ;
            DEBUGPRINTF( DEBUGLEVEL_DEBUG, ( "Brightness: %ld", Brightness ) ) ;
            Error = AdjustBrightness( MyCamera, Brightness ) ;
            if( !( Error ) )
            {  /* could adjust brightness */
              *ErrorCode = 0 ;
            }
            else
            {  /* could not adjust brightness */
            }
          }
          else
          {  /* camera could not be paused */
          }
        }
        else
        {  /* camera gone */
        }
      }
      if( ( Error ) || ( !( MyCamera->cc_Online ) ) )
      {  /* some error or camera gone while grabbing */
        DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "camera gone while grabbing" ) ) ;
        if( Digitize->custom_memhandling )
        {  /* use custom functions */
          Digitize->CstFreeVec( GrabedImage->chunky ) ;
          Digitize->CstFreeVec( GrabedImage ) ;
        }
        else
        {  /* use exec functions */
          FreeVec( GrabedImage->chunky ) ;
          FreeVec( GrabedImage ) ;
        }
        GrabedImage = NULL ;
        *ErrorCode = VHI_ERR_ERR_WHILE_DIG ;
      }
    }
    else
    {  /* vhi image not ok */
      DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "vhi image not ok" ) ) ;
      *ErrorCode = VHI_ERR_OUT_OF_MEMORY ;
    }
    StopCamera( MyCamera ) ;
  }
  else
  {  /* camera failed to start grabbing */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "camera gone before grabbing" ) ) ;
    *ErrorCode = VHI_ERR_NO_HARDWARE ;
  }

  return( GrabedImage ) ;
}


/*
** camera shall do something
*/
static ULONG vhi_method_perform( struct CameraContext *MyCamera, ULONG Item, APTR Attr, ULONG *ErrorCode )
{
  struct PrivateBase *PrivateBase ;
  ULONG Result ;
  struct vhi_dimensions *Dimensions ;
  struct vhi_image *VHIImage ;  

  PrivateBase = MyCamera->cc_PrivateBase ;
  Result = 0 ;
  
  switch( Item )
  {  /* what shall we do? */
    case VHI_CHECK_DIGITIZE_SIZE:
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "VHI_CHECK_DIGITIZE_SIZE" ) ) ;
      Dimensions = Attr ;
      if( NULL != Dimensions )
      {
#if 0
        if( ( ( LONG )Dimensions->x1 ) < 0 )
        {
          Dimensions->x1 = 0 ;
        }
        if( ( ( LONG )Dimensions->y1 ) < 0 )
        {
          Dimensions->y1 = 0 ;
        }
        if( Dimensions->x2 > ( 352 - 1 ) )
        {
          Dimensions->x2 = ( 352 - 1 ) ;
        }
        if(Dimensions->y2 >= ( 288- 1 ) )
        {
          Dimensions->y2 = ( 288- 1 ) ;
        }
        Dimensions->dst_width = 0 ;
        Dimensions->dst_height = 0 ;
#else
        Dimensions->x1 = 0 ;
        Dimensions->y1 = 0 ;
        Dimensions->x2 = ( 352 - 1 ) ;
        Dimensions->y2 = ( 288- 1 ) ;
        Dimensions->dst_width = 0 ;
        Dimensions->dst_height = 0 ;
#endif
      }
      break ;
    case VHI_DIGITIZE_PICTURE:
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "VHI_DIGITIZE_PICTURE" ) ) ;
      if( TRUE == MyCamera->cc_Online )
      {  /* camera seems still be there, so go on */
        VHIImage = vhi_grab( MyCamera, Attr, ErrorCode ) ;
        Result = ( ULONG )VHIImage ;
      }
      else
      {  /* camera seems already gone, so don't go on */
        *ErrorCode = VHI_ERR_NO_HARDWARE ;
      }
      break ;
    default:
      *ErrorCode = VHI_ERR_UNKNOWN_OPTION ;
      break ;
  }

  return( Result ) ;
}


/*
** vhi interface function
*/
ULONG REGFUNC vhi_api( REG( d2, ULONG method ), REG( d3, ULONG submethod ), REG( d4, APTR attr ), REG( d5, ULONG *errcode ), REG( d6, APTR vhi_handle), REG( a6, struct PrivateBase *PrivateBase ) )
{
  struct ExecBase *SysBase ;
  ULONG Result ;
  ULONG ErrorCode ;
  struct CameraContext *MyCamera ;

  SysBase = PrivateBase->pb_SysBase ;

  if( NULL == errcode )
  {  /* avoid crashes, if NULL is supplied as "error-pointer" */
    errcode = &ErrorCode ;
  }
  MyCamera = vhi_handle ;
  Result = 0 ;  /* our default result */

  switch( method )
  {  /* what method was specified */
    case VHI_METHOD_OPEN:
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "VHI_METHOD_OPEN" ) ) ;
      /* the value returned is stored and passed in vhi_handle at every call */
      MyCamera = AllocCamera( PrivateBase, errcode ) ;
      if( NULL != MyCamera )
      {  /* camera ok */
        Result = ( ULONG )MyCamera ;
      }
      break;
    case VHI_METHOD_GET:
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "VHI_METHOD_GET" ) ) ;
      Result = vhi_method_get( MyCamera, submethod, attr, errcode ) ;
      break;
    case VHI_METHOD_SET:
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "VHI_METHOD_SET" ) ) ;
      Result = vhi_method_set( MyCamera, submethod, attr, errcode ) ;
      break;
    case VHI_METHOD_PERFORM:
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "VHI_METHOD_PERFORM" ) ) ;
      Result = vhi_method_perform( MyCamera, submethod, attr, errcode ) ;
      break;
    case VHI_METHOD_CLOSE:
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "VHI_METHOD_CLOSE" ) ) ;
      /* free all ressources allocated at VHI_METHOD_OPEN */
      if( NULL != MyCamera )
      {  /* something can be freed */
        FreeCamera( MyCamera ) ;
      }
      break;
    default:
      *errcode = VHI_ERR_UNKNOWN_METHOD ;
      break;
  }
  
  return( Result ) ;
}
