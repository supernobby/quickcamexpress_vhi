/*
 * PB010x.h
 */


#ifndef _PB010X_H
#define _PB010X_H


#include "QuickCamExpress.h"


#define PB_I2C_ADDRESS ( 0xBA )


#define PB_R0_ChipVersion ( 0x00U )
#define PB_R1_RowWindowStart ( 0x01U )
#define PB_R2_ColumnWindowStart ( 0x02U )
#define PB_R3_RowWindowSize ( 0x03U )
#define PB_R4_ColumnWindowSize ( 0x04U )
#define PB_R5_ColumnFillIn ( 0x05U )
#define PB_R6_VerticalBlankCount ( 0x06U )
#define PB_R7_ControlMode ( 0x07U )
#define PB_R8_FrameUnitCount ( 0x08U )
#define PB_R9_RowUnitCount ( 0x09U )
#define PB_R10_RowSpeedControl ( 0x0AU )
#define PB_R11_AbortFrame ( 0x0BU )
#define PB_R12_Reserved ( 0x0CU )
#define PB_R13_Reset ( 0x0DU )
#define PB_R14_ExposureGainCommand ( 0x0EU )
#define PB_R15_Expose0 ( 0x0FU )
#define PB_R16_Expose1 ( 0x10U )
#define PB_R17_Expose2 ( 0x11U )
#define PB_R18_Low0DAC ( 0x12U )
#define PB_R19_Low1DAC ( 0x13U )
#define PB_R20_Low2DAC ( 0x14U )
#define PB_R21_Threshold11 ( 0x15U )
#define PB_R22_Threshold0x ( 0x16U )
#define PB_R23_UpdateInterval ( 0x17U )
#define PB_R24_HighDAC ( 0x18U )
#define PB_R25_Trans0H ( 0x19U )
#define PB_R26_Trans1L ( 0x1AU )
#define PB_R27_Trans1H ( 0x1BU )
#define PB_R28_Trans2L ( 0x1CU )
#define PB_R29_Reserved ( 0x1DU )
#define PB_R30_Reserved ( 0x1EU )
#define PB_R31_WaitToRead ( 0x1FU )
#define PB_R32_PixelReadControlMode ( 0x20U )
#define PB_R33_IREF_VLN ( 0x21U )
#define PB_R34_IREF_VLP ( 0x22U )
#define PB_R35_IREF_VLN_INTEG ( 0x23U )
#define PB_R36_IREF_MASTER ( 0x24U )
#define PB_R37_IDACP ( 0x25U )
#define PB_R38_IDACN ( 0x26U )
#define PB_R39_DAC_Control_Reg ( 0x27U )
#define PB_R40_VCL ( 0x28U )
#define PB_R40_IREF_VLN_ADCIN ( 0x29U )
#define PB_R42_Reserved ( 0x2AU )
#define PB_R43_Green1Gain ( 0x2BU )
#define PB_R44_BlueGain ( 0x2CU )
#define PB_R45_RedGain ( 0x2DU )
#define PB_R46_Green2Gain ( 0x2EU )
#define PB_R47_DarkRowAddress ( 0x2FU )
#define PB_R48_DarkRowOptions ( 0x30U )
#define PB_R49_Reserved ( 0x31U )
#define PB_R50_ImageTestData ( 0x32U )
#define PB_R51_MaximumGain ( 0x33U )
#define PB_R52_MinimumGain ( 0x34U )
#define PB_R53_GlobalGain ( 0x35U )
#define PB_R54_MaximumFrame ( 0x36U )
#define PB_R55_MinimumFrame ( 0x37U )
#define PB_R56_Reserved ( 0x38U )
#define PB_R57_VOFFSET ( 0x39U )
#define PB_R58_SnapShotSequenceTrigger ( 0x3AU )
#define PB_R59_VREF_HI ( 0x3BU )
#define PB_R60_VREF_LO ( 0x3CU )
#define PB_R61_Reserved ( 0x3DU )
#define PB_R62_Reserved ( 0x3EU )
#define PB_R63_Reserved ( 0x3FU )
#define PB_R64_RedBlueGain ( 0x40U )
#define PB_R65_Green2Green1Gain ( 0x41U )
#define PB_R66_VREF_HILO ( 0x42U )
#define PB_R67_RowUnitCount ( 0x43U )
#define PB_R240_ADCTest ( 0xF0U )
#define PB_R241_ChipEnable ( 0xF1U )
#define PB_R242_Reserved ( 0xF2U )


/*
 * module functions
 */
LONG InitPB010x( struct CameraContext *MyCamera ) ;


#endif  // !_PB010X_H
