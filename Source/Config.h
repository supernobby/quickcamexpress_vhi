/*
** Config.h
*/


#ifndef _CONFIG_H
#define _CONFIG_H


#include <exec/execbase.h>


BYTE ReadIntVariable( struct ExecBase *SysBase, unsigned char *Name, void *Buffer, unsigned long Size ) ;
BYTE ReadStringVariable( struct ExecBase *SysBase, unsigned char *Name, void *Buffer, unsigned long Size ) ;


#endif  /* _CONFIG_H */