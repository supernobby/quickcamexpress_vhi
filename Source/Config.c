/*
** Config.c
*/


#include "Config.h"
#include "DebugOutput.h"
#include "ab_stdlib.h"
#include <proto/exec.h>
#include <proto/dos.h>
#include <stdlib.h>


/*
** buffer size for GetVar() buffer
*/
#define STRINGBUFFER_SIZE ( 256 )


/*
** load integer environment variable 
*/
BYTE ReadIntVariable( struct ExecBase *SysBase, unsigned char *Name, void *Buffer, unsigned long Size )
{
  struct Task *CallerTask ;
  struct DosLibrary *DOSBase ;
  unsigned char *StringBuffer ;
  signed long Length, Value ;
  BYTE Result ;
  
  Result = 1 ;
  CallerTask = FindTask( NULL ) ;
  if( NULL != CallerTask )
  {
    if( NT_PROCESS == CallerTask->tc_Node.ln_Type )
    {  /* we can use dos.library */
      DOSBase = ( struct DosLibrary * )OpenLibrary( "dos.library", 37 ) ;
      if( NULL != DOSBase )
      {  /* dos.library ok */
        StringBuffer = AllocMem( STRINGBUFFER_SIZE, ( MEMF_PUBLIC | MEMF_CLEAR ) ) ;
        if( NULL != StringBuffer )
        {  /* string buffer ok */
          Length = GetVar( Name, StringBuffer, STRINGBUFFER_SIZE, ( GVF_GLOBAL_ONLY ) ) ;
          if( ( 0 <= Length ) )
          {  /* set variable */
            Value = ab_atol( StringBuffer ) ;
            switch( Size )
            {
              case 1:
                *( ( signed char * )Buffer ) = ( signed char )Value ;
                break ;
              case 2:
                *( ( signed short * )Buffer ) = ( signed short )Value ;
                break ;
              case 4:
                *( ( signed long * )Buffer ) = ( signed long )Value ;
                break ;
            }
            Result = 0 ;
          }
          else
          {  /* leave variable untouched */
          }          
          FreeMem( StringBuffer, STRINGBUFFER_SIZE ) ;
        }
        else
        {  /* string buffer not ok */
        }
        CloseLibrary( ( struct Library * )DOSBase ) ;
      }
      else
      {  /* dos.library not ok */
      }
    }
    else
    {  /* we can't use dos.library */
    }
  }
  else
  {
  }
  
  return( Result ) ;
}



/*
** load integer environment variable 
*/
BYTE ReadStringVariable( struct ExecBase *SysBase, unsigned char *Name, void *Buffer, unsigned long Size )
{
  struct Task *CallerTask ;
  struct DosLibrary *DOSBase ;
  signed long Length ;
  BYTE Result ;
  
  Result = 1 ;
  CallerTask = FindTask( NULL ) ;
  if( NULL != CallerTask )
  {
    if( NT_PROCESS == CallerTask->tc_Node.ln_Type )
    {  /* we can use dos.library */
      DOSBase = ( struct DosLibrary * )OpenLibrary( "dos.library", 37 ) ;
      if( NULL != DOSBase )
      {  /* dos.library ok */
        Length = GetVar( Name, Buffer, Size, ( GVF_GLOBAL_ONLY ) ) ;
        if( ( 0 <= Length ) )
        {  /* variable read */
          Result = 0 ;
        }
        else
        {  /* leave variable untouched */
        }          
        CloseLibrary( ( struct Library * )DOSBase ) ;
      }
      else
      {  /* dos.library not ok */
      }
    }
    else
    {  /* we can't use dos.library */
    }
  }
  else
  {
  }

  return( Result ) ;
}
