/*
** HDCS1020.h
*/


#ifndef _HDCS1020_H
#define _HDCS1020_H


#include "QuickCamExpress.h"


/*
** the sensor i2c address
*/
#define HDCS_I2C_ADDRESS ( 0x55UL << 1 )  /* 0xAA */


/*
** register addesses
*/
#define HDCS_IDENT ( 0x00UL << 1 )  /* 0x00 */
#define HDCS_STATUS ( 0x01UL << 1 )  /* 0x2 */
#define HDCS_IMASK ( 0x02UL << 1 )  /* 0x4 */
#define HDCS_PCTRL ( 0x03UL << 1 )  /* 0x6 */
#define HDCS_PDRV ( 0x04UL << 1 )  /* 0x8 */
#define HDCS_ICTRL ( 0x05UL << 1 )  /* 0xA */
#define HDCS_ITMG ( 0x06UL << 1 )  /* 0xC */
#define HDCS_BFRAC ( 0x07UL << 1 )  /* 0xE */
#define HDCS_BRATE ( 0x08UL << 1 )  /* 0x10 */
#define HDCS_ADCCTRL ( 0x09UL << 1 )  /* 0x12 */
#define HDCS_FWROW ( 0x0AUL << 1 )  /* 0x14 */
#define HDCS_FWCOL ( 0x0BUL << 1 )  /* 0x16 */
#define HDCS_LWROW ( 0x0CUL << 1 )  /* 0x18 */
#define HDCS_LWCOL ( 0x0DUL << 1 )  /* 0x1A */
#define HDCS_TCTRL ( 0x0EUL << 1 )  /* 0x1C */
#define HDCS_ERECPGA ( 0x0FUL << 1 )  /* 0x1E */
#define HDCS_EROCPGA ( 0x10UL << 1 )  /* 0x20 */
#define HDCS_ORECPGA ( 0x11UL << 1 )  /* 0x22 */
#define HDCS_OROCPGA ( 0x12UL << 1 )  /* 0x24 */
#define HDCS_ROWEXPL ( 0x13UL << 1 )  /* 0x26 */
#define HDCS_ROWEXPH ( 0x14UL << 1 )  /* 0x28 */
#define HDCS_SROWEXP ( 0x15UL << 1 )  /* 0x2A */
#define HDCS_ERROR ( 0x16UL << 1 )  /* 0x2C */
#define HDCS_ITMG2 ( 0x17UL << 1 )  /* 0x2E */
#define HDCS_ICTRL2 ( 0x18UL << 1 )  /* 0x30 */
#define HDCS_HBLANK ( 0x19UL << 1 )  /* 0x32 */
#define HDCS_VBLANK ( 0x1AUL << 1 )  /* 0x34 */
#define HDCS_CONFIG ( 0x1BUL << 1 )  /* 0x36 */
#define HDCS_CONTROL ( 0x1CUL << 1 )  /* 0x38 */


/*
** Identifications Register IDENT 0x00
*/
#define HDCS_IDENT_REV ( 0x07U<<0 )
#define HDCS_IDENT_TYPE ( 0x1FU<<1 )


/*
** Status Register STATUS 0x01
*/
#define HDCS_STATUS_RF ( 1U<<0 )
#define HDCS_STATUS_RC ( 1U<<1 )
#define HDCS_STATUS_FC ( 1U<<2 )
#define HDCS_STATUS_CC ( 1U<<3 )
#define HDCS_STATUS_EFS ( 1U<<4 )
#define HDCS_STATUS_SFS ( 1U<<5 )
#define HDCS_STATUS_SSF ( 1U<<6 )


/*
** Interrupt Mask Register IMASK 0x02
*/
#define HDCS_IMASK_IEN ( 1U<<0 )
#define HDCS_IMASK_IRC ( 1U<<1 )
#define HDCS_IMASK_IFC ( 1U<<2 )
#define HDCS_IMASK_ICC ( 1U<<3 )
#define HDCS_IMASK_IEFS ( 1U<<4 )
#define HDCS_IMASK_ISFS ( 1U<<5 )
#define HDCS_IMASK_ISS ( 1U<<6 )


/*
** Pad Control Register PCTRL 0x03
*/
#define HDCS_PCTRL_RCE ( 1U<<0 )
#define HDCS_PCTRL_FSE ( 1U<<1 )
#define HDCS_PCTRL_FSS ( 1U<<2 )
#define HDCS_PCTRL_ICE ( 1U<<3 )
#define HDCS_PCTRL_IPD ( 1U<<4 )
#define HDCS_PCTRL_LVR ( 1U<<5 )
#define HDCS_PCTRL_LVF ( 1U<<6 )
#define HDCS_PCTRL_LVC ( 1U<<7 )


/*
** Pad Drive Control Register PDRV 0x04
*/
#define HDCS_PDRV_DATDRV_0 ( 1U<<0 )
#define HDCS_PDRV_DATDRV_1 ( 1U<<1 )
#define HDCS_PDRV_RDYDRV_0 ( 1U<<2 )
#define HDCS_PDRV_RDYDRV_1 ( 1U<<3 )
#define HDCS_PDRV_STATDRV_0 ( 1U<<4 )
#define HDCS_PDRV_STATDRV_1 ( 1U<<5 )
#define HDCS_PDRV_TXDDRV_0 ( 1U<<6 )
#define HDCS_PDRV_TXDDRV_1 ( 1U<<7 )


/*
** Interface Control Register ICTRL 0x05
*/
#define HDCS_ICTRL_AAD ( 1U<<0 )
#define HDCS_ICTRL_DAD ( 1U<<1 )
#define HDCS_ICTRL_DOD_0 ( 1U<<2 )
#define HDCS_ICTRL_DOD_1 ( 1U<<3 )
#define HDCS_ICTRL_DDO ( 1U<<4 )
#define HDCS_ICTRL_DSC_0 ( 1U<<5 )
#define HDCS_ICTRL_DSC_1 ( 1U<<6 )
#define HDCS_ICTRL_HAVG ( 1U<<7 )


/*
** Interface Timing Register ITMG 0x06
*/
#define HDCS_ITMG_RPC_0 ( 1U<<0 )
#define HDCS_ITMG_RPC_1 ( 1U<<1 )
#define HDCS_ITMG_RPC_2 ( 1U<<2 )
#define HDCS_ITMG_DHC_0 ( 1U<<3 )
#define HDCS_ITMG_DHC_1 ( 1U<<4 )
#define HDCS_ITMG_DPS ( 1U<<5 )


/*
** 
*/
#define HDCS_0 ( 1U<<0 )
#define HDCS_1 ( 1U<<1 )
#define HDCS_2 ( 1U<<2 )
#define HDCS_3 ( 1U<<3 )
#define HDCS_4 ( 1U<<4 )
#define HDCS_5 ( 1U<<5 )
#define HDCS_6 ( 1U<<6 )
#define HDCS_7 ( 1U<<7 )


/*
** Error Control Register ERROR 0x16
*/
#define HDCS_ERROR_EEF ( 1U<<0 )
#define HDCS_ERROR_IEE ( 1U<<1 )
#define HDCS_ERROR_IEF ( 1U<<2 )
#define HDCS_ERROR_IIE ( 1U<<3 )


/*
** Configuration Register CONFIG 0x1B
*/
#define HDCS_CONFIG_MODE ( 1U<<0 )
#define HDCS_CONFIG_SEN ( 1U<<1 )
#define HDCS_CONFIG_SFC ( 1U<<2 )
#define HDCS_CONFIG_CFC ( 1U<<3 )
#define HDCS_CONFIG_CSS ( 1U<<4 )
#define HDCS_CONFIG_RSS ( 1U<<5 )
#define HDCS_CONFIG_SDOE ( 1U<<6 )


/*
** Control Register CONTROL 0x1C
*/
#define HDCS_CONTROL_RST ( 1U<<0 )
#define HDCS_CONTROL_SLP ( 1U<<1 )
#define HDCS_CONTROL_RUN ( 1U<<2 )
#define HDCS_CONTROL_ARST ( 1U<<3 )
#define HDCS_CONTROL_LCK ( 1U<<4 )
#define HDCS_CONTROL_PWR ( 1U<<5 )


/*
** functions exported by the module
*/
LONG InitHDCS1020( struct CameraContext *MyCamera ) ;


#endif  /* _HDCS1020_H */
