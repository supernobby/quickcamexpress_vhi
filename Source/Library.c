/*
** Library.c
*/


#include "Library.h"
#include "QuickCamExpress.h"
#include <proto/exec.h>
#include <exec/initializers.h>
#include <exec/resident.h>
#include <dos/dosextens.h>


/*
** the version tag
*/
static const char VersionTag[ ] = "\0$VER: " NAME_STRING " " MAJOR_STRING "." MINOR_STRING "." REVISION_STRING " (" DATE_STRING ") " MISC_STRING ;


/*
** entry function when called from CLI or Workbench
*/
int DoNotCrashStartup( void )
{
  /* 
  ** for StormC4 this must be the first function in the first module
  ** passed to the linker to be included in the output
  */

  return( -1 ) ;
}


/*
** function tabel of the library, also used in InitTable[]
*/
static const APTR FunctionTable[ ] =
{
  /* OS functions */
  ( APTR )LibOpen,
  ( APTR )LibClose,
  ( APTR )LibExpunge,
  ( APTR )LibReserved,
  /* vhi library functions */
  ( APTR )vhi_api,
  ( APTR )-1
} ;


/*
** InitStruct() compatible data, required by InitTable[]
*/
static const UWORD DataTable[] =
{
  INITBYTE(OFFSET(Node,ln_Type),NT_LIBRARY),
  INITLONG(OFFSET(Node,ln_Name),0),
  INITBYTE(OFFSET(Library,lib_Flags),LIBF_SUMUSED|LIBF_CHANGED),
  INITWORD(OFFSET(Library,lib_Version),QUICKCAMEXPRESS_VHI_MAJOR),
  INITWORD(OFFSET(Library,lib_Revision),QUICKCAMEXPRESS_VHI_MINOR),
  INITLONG(OFFSET(Library,lib_IdString),0),
  INITWORD(OFFSET(Library,lib_OpenCnt),0),  /* set to 1 to permanently prevent expunge */
  0
} ;


/*
** called from the "ROMTag", required by InitTable[]
*/
static struct Library *InitFunc( REG( d0, struct PrivateBase *PrivateBase ), REG( a0, BPTR SegList ), REG( a6, struct ExecBase *SysBase ) )
{
  struct Library *Result ;

  Result = NULL ;

  /* strings not compatible with InitStruct()?!?, so do this manually */
  PrivateBase->pb_Library.lib_Node.ln_Name = NAME_STRING ;
  PrivateBase->pb_Library.lib_IdString = ( char * )&VersionTag[ 7 ] ;

  /* store important values for later use */
  PrivateBase->pb_SysBase = SysBase ;
  PrivateBase->pb_SegList = SegList ;

  /* all ok */
  Result = &PrivateBase->pb_Library ;
  
  /* when returning the pointer to the library base, things are ok */
  return( Result ) ;
}


/*
** table to be suitable for the RTF_AUTOINIT flag in the ROMTag
*/
static const ULONG InitTable[ 4 ] =
{
  sizeof( struct PrivateBase ),  /* size of library base structure */
  ( ULONG )FunctionTable,  /* pointer to function table */
  ( ULONG )DataTable,  /* pointer to data table for InitStruct function */
  ( ULONG )InitFunc  /* pointer to initialization code */
} ;


/*
** the ROMTag structure that we can run from (FLASH) ROM
*/
static const struct Resident ROMTag =
{
  RTC_MATCHWORD, /* rt_MatchWord word to match on (ILLEGAL) */
  &ROMTag,  /* struct Resident *rt_MatchTag; pointer to the above */
  ( &ROMTag ) + 1,  /* rt_EndSkip; address to continue scan */
  RTF_COLDSTART | RTF_AUTOINIT,  /* rt_Flags;  various tag flags */
  QUICKCAMEXPRESS_VHI_MAJOR,  /* rt_Version; release version number */
  NT_LIBRARY,  /* rt_Type; type of module (NT_XXXXXX) */
  0, /* rt_Pri; initialization priority */
  NAME_STRING,  /* *rt_Name; pointer to node name */
  ( char * )&VersionTag[ 7 ],  /* *rt_IdString; pointer to identification string */
  InitTable  /* rt_Init; pointer to init code */
} ;


/*
** must return 0
*/
ULONG LibReserved( void )
{
  return( 0 ) ;
}


/*
** called via RemLibrary() or automatically if system is out of memory
*/
BPTR LibExpunge( REG( a6, struct PrivateBase *PrivateBase ) )
{
  struct ExecBase *SysBase ;
  BPTR Result ;

  SysBase = PrivateBase->pb_SysBase ;

  if( 0 == PrivateBase->pb_Library.lib_OpenCnt )
  {  /* library unused, expunge can be done */
    /* segments can be unloaded afterwards */
    Result = PrivateBase->pb_SegList ;
    /* unlink from list before we free the node memory */
    Remove( &PrivateBase->pb_Library.lib_Node ) ;

    /* ... library specific closings would go here ... */

    /* free our memory */
    FreeMem( ( ( BYTE * )PrivateBase ) - PrivateBase->pb_Library.lib_NegSize, \
      ( ULONG )( PrivateBase->pb_Library.lib_NegSize + PrivateBase->pb_Library.lib_PosSize ) ) ;
  }
  else
  {  /* library still in use */
    /* set the delayed expunge flag */
    PrivateBase->pb_Library.lib_Flags |= ( LIBF_DELEXP ) ;
    /* segments can't be unloaded */
    Result = 0 ;
  }

  return( Result ) ;
}


/*
** called via CloseLibrary()
*/
BPTR LibClose( REG( a6, struct PrivateBase *PrivateBase ) )
{
  struct ExecBase *SysBase ;
  BPTR Result ;

  SysBase = PrivateBase->pb_SysBase ;
  Result = 0 ;

  /* call custom close for this library */
  CustomLibClose( PrivateBase ) ;

  /* decrement open counter of the library node */
  PrivateBase->pb_Library.lib_OpenCnt-- ;
  if( 0 == PrivateBase->pb_Library.lib_OpenCnt )
  {  /* library is now unused */
    if( LIBF_DELEXP & PrivateBase->pb_Library.lib_Flags )
    {  /* expunge pending */
      Result = LibExpunge( PrivateBase ) ;
    }
    else
    {  /* no expunge pending, so leave the library in the system */
    }
  }
  else
  {  /* library still in use */
  }

  return( Result ) ;
}


/*
** called via OpenLibrary()
*/
struct Library *LibOpen( REG( a6, struct PrivateBase *PrivateBase ) )
{
  struct ExecBase *SysBase ;
  struct Library *Result ;
  
  SysBase = PrivateBase->pb_SysBase ;
  Result = NULL ;
  
  /* call custom open for this library */
  Result = CustomLibOpen( PrivateBase ) ;

  if( NULL != Result )
  {  /* custom part is ok */
    PrivateBase->pb_Library.lib_OpenCnt++ ;
    PrivateBase->pb_Library.lib_Flags &= ~( LIBF_DELEXP ) ;
  }
  else
  {  /* custom part not ok */
  }

  return( Result ) ;
}
