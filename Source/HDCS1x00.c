/*
** HDCS1x00.h
*/


#include "HDCS1x00.h"
#include "STV0602AA.h"
#include "DebugOutput.h"
#include <stdio.h>



/*
** start sensor to capture images
*/
static LONG StartSensor( struct CameraContext *MyCamera )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;

  Error = WriteSensor( MyCamera, HDCS1X00_CONTROL, HDCS1X00_CONTROL_RUN, 1 ) ;  /* clear power down and set run mode */
  if( 0 == Error )
  {  /* setting sensor to run mode ok */
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "start sensor ok" ) ) ;
  }
  else
  {  /* setting sensor to run mode failed */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "start sensor failed" ) ) ;
  }

  return( Error ) ;
}


/*
** stop sensor to capture images
*/
static LONG StopSensor( struct CameraContext *MyCamera )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;

  PrivateBase = MyCamera->cc_PrivateBase ;

  Error = WriteSensor( MyCamera, HDCS1X00_CONTROL, 0, 1 ) ;  /* clear run mode and go in power down */
  if( 0 == Error )
  {  /* disabling sensor run mode ok */
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "stop sensor ok" ) ) ;
  }
  else
  {  /* disabling sensor run mode failed */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "stop sensor failed" ) ) ;
  }

  return( Error ) ;
}



/*
 * some timing values
 */
#define F ( 25 )  /* system clock frequency in MHz */
#define RS ( 186 )  /* row sample period constant */
#define CPO ( 2 )  /* column processing overhead */
#define NCP ( 352 )  /* number of columns processed */
#define CTO ( 4 )  /* column timing overhead */
#define PS ( 5 )  /* PGA sample signal duration, PSMP field of TCTRL reg */
#define AS ( 5 )  /* ADC start signal duration, ASTRT field of TCTRL reg */
#define SCSF ( 1 )  /* single channel scale factor */
#define CT ( CTO + PS + AS )  /* column timing period */
#define CP ( CPO + ( NCP * CT / SCSF ) )  /* column processing period */
#define RP ( RS + CP )  /* row processing period */
#define ER ( 100 )  /* exposure reset duration */
#define MNCT ( ( ER + 5 + ( CT - 1 ) ) / CT )


/*
 * set exposure time
 */
static LONG SetExposure( struct CameraContext *MyCamera, ULONG NanoSeconds )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;
  LONG ROWEXP ;
  LONG SROWEXP ;

  PrivateBase = MyCamera->cc_PrivateBase ;
  Error = 0 ;
  
  ROWEXP = NanoSeconds * F / RP ;
  if( 32767 < ROWEXP )
  {
    ROWEXP = 32767 ;
  }
  SROWEXP = ( CP - ER - 6 - ( ( NanoSeconds * F ) - ( ROWEXP * RP ) ) ) ;
  if( 0 > SROWEXP )
  {
    SROWEXP = 0 ;
  }
  else if( ( CP - ( MNCT * CT ) - 1 ) < SROWEXP )
  {
    SROWEXP = ( CP - ( MNCT * CT ) - 1 ) ;
  }
  
  DEBUGPRINTF( DEBUGLEVEL_DEBUG, ( "NanoSeconds: %ld, ROWEXP: %ld, SROWEXP: %ld", NanoSeconds, ROWEXP, SROWEXP ) ) ;
  WriteSensor( MyCamera, HDCS1X00_ROWEXPL, ( ( ROWEXP >> 0 ) & 0xFF ), 1 ) ;
  WriteSensor( MyCamera, HDCS1X00_ROWEXPH, ( ( ROWEXP >> 8 ) & 0xFF ), 1 ) ;
  WriteSensor( MyCamera, HDCS1X00_SROWEXPL, ( ( SROWEXP >> 0 ) & 0xFF ), 1 ) ;
  WriteSensor( MyCamera, HDCS1X00_SROWEXPH, ( ( SROWEXP >> 8 ) & 0xFF ), 1 ) ;
  WriteSensor( MyCamera, HDCS1X00_STATUS, HDCS1X00_STATUS_EEF, 1 ) ;

  MyCamera->cc_Exposure = NanoSeconds ;

  return( Error ) ;
}


/*
** initialize HDCS1x00
*/
LONG InitHDCS1x00( struct CameraContext *MyCamera )
{
  struct PrivateBase *PrivateBase ;
  LONG Error ;
  UBYTE SensorIdent ;
  
  PrivateBase = MyCamera->cc_PrivateBase ;

  /* set I2C registers to 8-bit */
  Error = WriteBridge( MyCamera, STV_0x1423, 0, 1 ) ;
  if( !( Error ) )
  {  /* 8-bit I2C registers could be enabled */
    MyCamera->cc_SensorAddress = HDCS1X00_I2C_ADDRESS ;
    Error = ReadSensor( MyCamera, HDCS1X00_IDENT, &SensorIdent, 1 ) ;
    DEBUGPRINTF( DEBUGLEVEL_INFO, ( "HDCS1X00_IDENT: 0x%lX", ( ULONG )SensorIdent ) ) ;
    if( ( !( Error ) ) && ( 0x08 == SensorIdent ) )
    {  /* sensor identification ok */
      DEBUGPRINTF( DEBUGLEVEL_INFO, ( "HDCS1x00 sensor detected" ) ) ;
      MyCamera->cc_StartSensor = StartSensor ;
      MyCamera->cc_StopSensor = StopSensor ;
      MyCamera->cc_SetExposure = SetExposure ;
      WriteSensor( MyCamera, HDCS1X00_CONTROL, HDCS1X00_CONTROL_RST, 1 ) ;  /* do sensor reset */
      WriteSensor( MyCamera, HDCS1X00_CONTROL, 0x00, 1 ) ;  /* disable default power down mode */
      /* set some registers to HDCS-1020 default values that are 0 in the HDCS-1000 */
      WriteSensor( MyCamera, HDCS1X00_PCTRL, 0x1B, 1 ) ;  /* HDCS-1020 reset value */
      WriteSensor( MyCamera, HDCS1X00_ICTRL, 0x60, 1 ) ;  /* HDCS-1020 reset value */
      WriteSensor( MyCamera, HDCS1X00_ITMG, 0x1A, 1 ) ;  /* HDCS-1020 reset value */
      WriteSensor( MyCamera, HDCS1X00_ROWEXPL, 0x2C, 1 ) ;  /* HDCS-1020 reset value */
      WriteSensor( MyCamera, HDCS1X00_ROWEXPH, 0x01, 1 ) ;  /* HDCS-1020 reset value */
      WriteSensor( MyCamera, HDCS1X00_CONFIG, 0x0C, 1 ) ;  /* HDCS-1020 reset value */
#if 0      
      /* these were required for the HDCS-1020 to produce images */      
      WriteSensor( MyCamera, HDCS1X00_PCTRL, 0x63, 1 ) ;  /* enable some pins */
      WriteSensor( MyCamera, HDCS1X00_TCTRL, 0xCB, 1 ) ;  /*  */
      WriteSensor( MyCamera, HDCS1X00_ITMG, 0x16, 1 ) ;  /*  */
#endif
      WriteSensor( MyCamera, HDCS1X00_STATUS, 0x7E, 1 ) ;  /* clear status register */
      WriteSensor( MyCamera, HDCS1X00_IMASK, 0x00, 1 ) ;  /* disable interrupts */
      WriteSensor( MyCamera, HDCS1X00_PCTRL, 0x63, 1 ) ;  /* enable some pins */
      WriteSensor( MyCamera, HDCS1X00_PDRV, 0x00, 1 ) ;  /* all pins: high drive (5 ns) */
      WriteSensor( MyCamera, HDCS1X00_ICTRL, 0x20, 1 ) ;  /* DSC: 1 clocks */
      WriteSensor( MyCamera, HDCS1X00_ITMG, 0x12, 1 ) ;  /* DHC: 2 clocks, RPC: 3 clocks */
      WriteSensor( MyCamera, HDCS1X00_ADCCTRL, 0x0A, 1 ) ;  /* ARES: 10bit resolution */
      WriteSensor( MyCamera, HDCS1X00_CONFIG, 0x08, 1 ) ;  /* normal mode with continuous frame capture */
      WriteSensor( MyCamera, HDCS1X00_TCTRL, ( ( ( AS - 2 ) << 5 ) | ( PS ) ) /* 0x65 */, 1 ) ;  /* ASTRT = 3, PSMP = 5 */
      WriteSensor( MyCamera, HDCS1X00_FWROW, 0x03, 1 ) ;  /* fist row is 12 */
      WriteSensor( MyCamera, HDCS1X00_FWCOL, 0x03, 1 ) ;  /* first column 12 */
      WriteSensor( MyCamera, HDCS1X00_LWROW, 0x4A, 1 ) ;  /* last row 299 */
      WriteSensor( MyCamera, HDCS1X00_LWCOL, 0x5A, 1 ) ;  /* last column 263 */

      SetExposure( MyCamera, MyCamera->cc_Exposure ) ;
    }
    else
    {  /* sensor identification not ok */
       DEBUGPRINTF( DEBUGLEVEL_INFO, ( "no HDCSx10 sensor found" ) ) ;
       Error = -1 ;
    }
  }
  else
  {  /* 8-bit I2C registers could not be enabled */
    DEBUGPRINTF( DEBUGLEVEL_ERROR, ( "8-bit I2C registers could not be enabled" ) ) ;
  }
  
  return( Error ) ;
}

