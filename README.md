# quickcamexpress_vhi

This is software for the classic AmigaOS 3.x.
It should enable the Amiga to use an old Logitech Quickcam Express USB camera to capture images.

Technically it implements a VHI standard driver. When it gets opened by some application that supports the VHI drivers, the Poseidon USB stack is ask for a compatible camera. If one is found, the driver will do an application binding on it and image capture can start.
