/*
** DebugOutput.h
*/


#ifndef _DEBUGOUTPUT_H
#define _DEBUGOUTPUT_H


#include "Library.h"
#include <exec/execbase.h>


/*
** dynamic debug level
*/
#define DEBUGLEVEL ( PrivateBase->pb_DebugLevel )


/*
** valid debug levels
*/
#define DEBUGLEVEL_OFF ( 0 )
#define DEBUGLEVEL_FAILURE ( 1 )
#define DEBUGLEVEL_ERROR ( 2 )
#define DEBUGLEVEL_WARNING ( 3 )
#define DEBUGLEVEL_INFO ( 4 )
#define DEBUGLEVEL_DEBUG ( 5 )


void InitDebugOutput( struct ExecBase *SysBase ) ;
void DebugPrintf( const char *FormatString, ... ) ;


/*
** works only with C99
**
** #define TRACEPRINT( _FormatString, ... ) DebugPrintf( DEBUGLEVEL_TRACE, _FormatString, __VA_ARGS__ )
** #define ERRORPRINT( _FormatString, ... ) DebugPrintf( DEBUGLEVEL_ERROR, _FormatString, __VA_ARGS__ )
*/


#define DEBUGPRINTF( _level, _body ) if( DEBUGLEVEL >= _level ) DebugPrintf _body ;


#endif  /* _DEBUGOUTPUT_H */
